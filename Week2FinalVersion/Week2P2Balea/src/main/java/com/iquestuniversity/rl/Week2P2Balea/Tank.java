package com.iquestuniversity.rl.Week2P2Balea;

import java.util.Stack;

public class Tank {
	private int maxSize;
	private Stack<Object> stack;

	public Tank(int sizeOfTank) {
		stack = new Stack<Object>();
		maxSize = sizeOfTank;
	}

	public void addElementIntoTank(Object addedValue) {
		if (stack.size() == maxSize) {
			System.out.println("Can't add element into Tank,Tank is full");
		} else {
			stack.push(addedValue);
		}

	}

	public Object popFromTank() {
		if (stack.empty()) {
			System.out.println("Can't remove any element,stack is empty");
		} else {
			Object poppedElement = stack.pop();
			return poppedElement;
		}
		return null;
	}

	public boolean isEmpty() {
		if (stack.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected void finalize() throws Throwable {
		if (isEmpty()) {
			System.out.println("The tank is empty,GC can be called");
		} else {
			System.out.println("Tank stil has elements,GC can't be called,all elements will be erased to call GC");
			stack.removeAllElements();
			System.out.println("GC was called");
		}

	}

}
