package Main;

import com.iquestuniversity.rl.Week2P2Balea.Tank;

public class Main {

	public final static int MAX_SIZE = 10;

	public static void main(String[] args) {
		
		int numbersToAddInTank = 14;
		Tank tank = new Tank(MAX_SIZE);
		
		for (int number = 0; number < numbersToAddInTank; number++) {
			tank.addElementIntoTank(number);
		}

		while (!tank.isEmpty()) {
			Object poppedElement = tank.popFromTank();
			System.out.println("Popped element : " + poppedElement);
		}

		tank.addElementIntoTank(5);
		
		tank = null;
		System.gc();

	}

}
