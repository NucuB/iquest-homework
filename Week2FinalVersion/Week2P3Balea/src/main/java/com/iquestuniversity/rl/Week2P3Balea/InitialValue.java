package com.iquestuniversity.rl.Week2P3Balea;

public class InitialValue {

	public InitialValue() {
		System.out.println("Constructor with no parameters from InitialValue called !!!!");
		System.out.println(" ");
	}

	public InitialValue(String message) {
		System.out.println("Constructor with parameters from InitialValue says : " + message);
		System.out.println(" ");
	}

}
