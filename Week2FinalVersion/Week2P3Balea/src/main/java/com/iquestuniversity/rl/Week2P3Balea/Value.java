package com.iquestuniversity.rl.Week2P3Balea;

public class Value extends InitialValue {

	private int intVariable = initializeVariableWithValue(100);

	public Value(String message) {
		super("Hello from InitialValue");
		System.out.println("");
		System.out.println("Constructor with parameters from Value class says : " + message);

	}

	public Value() {
		System.out.println("I am the constructor with no parameters from Value class");
		System.out.println("");
	}

	private int initializeVariableWithValue(int value) {
		return value;
	}

	public void printMemberVariable() {
		System.out.println("Value of int variable is " + intVariable);
		System.out.println("");
	}

}
