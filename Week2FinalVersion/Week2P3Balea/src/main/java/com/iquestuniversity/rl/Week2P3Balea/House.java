package com.iquestuniversity.rl.Week2P3Balea;

public class House {

	private Window leftSideWindow;
	private Window frontSideWindow;
	private Window inlineInstantiatedWindow = new Window(4);
	public static Window rightSideWindow;

	{
		System.out.println("We are in the non-static block");
		leftSideWindow = new Window(2);
		System.out.println("Exit from non-static block!!!");
	}

	static {
		System.out.println("We are in the static block!!!");
		rightSideWindow = new Window(1);
		System.out.println("Exit from static block!!!");
	}

	public House() {
		System.out.println("We are in constructor of House class !!!");
		frontSideWindow = new Window(3);

	}

}
