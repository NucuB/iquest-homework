package Main;

import com.iquestuniversity.rl.Week2P1Balea.Person;

public class Application {

	public static void main(String[] args) {

		// Person person1 = new Person(); Doesn't work because I already wrote at least
		// one constructor with parameters.

		Person person1 = new Person("George Walker", "Bush");
		Person person2 = new Person("Abraham preNume1 preNume2 Lincoln");
		Person person3 = new Person("Nicusor Robert Balea");
		Person person4 = new Person("Ana Maria", "Ionescu");
		Person person5 = new Person("Jimmy Carter");
		Person person6 = new Person("Nicusor", "Balea");

		System.out.println(person1.toString());
		System.out.println(person2.toString());
		System.out.println(person3.toString());
		System.out.println(person4.toString());
		System.out.println(person5.toString());
		System.out.println(person6.toString());

	}

}
