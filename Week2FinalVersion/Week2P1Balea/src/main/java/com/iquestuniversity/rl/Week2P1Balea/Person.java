package com.iquestuniversity.rl.Week2P1Balea;

public class Person {

	private String surName;
	private String firstName;

	public Person(String firstName, String surName) {
		this.firstName = firstName;
		this.surName = surName;
	}

	public Person(String fullName) {

		String[] stringAfterSplitName = fullName.split(" ");
		surName = stringAfterSplitName[stringAfterSplitName.length - 1];
		firstName = extractFirstName(fullName);

	}

	private String extractFirstName(String fullName) {
		String[] stringAfterSplitName = fullName.split(" ");
		StringBuilder stringBuilder = new StringBuilder();
		for (int index = 0; index <= stringAfterSplitName.length - 2; index++) {
			stringBuilder.append(stringAfterSplitName[index]);
			stringBuilder.append(" ");
		}
		return stringBuilder.toString();
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String toString() {

		return "Person's Firstname(s) : " + firstName + " and Surname : " + surName;
	}

}
