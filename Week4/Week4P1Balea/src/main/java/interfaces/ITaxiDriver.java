package interfaces;

public interface ITaxiDriver extends IPerson {

	public void sleep();
	public String getCurrentLocation();
	public void goToAddress(String address);
}
