package interfaces;

public interface IPerson {
	
	public void eat();
	public String getName();
}
