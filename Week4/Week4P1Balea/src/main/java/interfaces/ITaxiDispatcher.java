package interfaces;

public interface ITaxiDispatcher extends IPerson {

	public void dispatch(String location);
}
