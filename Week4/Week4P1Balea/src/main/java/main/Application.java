package main;

import java.util.ArrayList;
import java.util.List;

import com.iQuestUniversity.rl.Week4P1Balea.DispatchOperator;
import com.iQuestUniversity.rl.Week4P1Balea.TaxiDriver;

import interfaces.ITaxiDriver;

public class Application {

	public static void main(String[] args) {
		
		List<ITaxiDriver> drivers = new ArrayList<ITaxiDriver>();
		TaxiDriver advancedDriver = new TaxiDriver("Vlad");
		TaxiDriver begginerDriver = new TaxiDriver("Andrei");
        drivers.add(advancedDriver);
        drivers.add(begginerDriver);
        DispatchOperator operator = new DispatchOperator("Daniel", drivers);

        operator.dispatch("Calea Motilor nr. 6");

	}

}
