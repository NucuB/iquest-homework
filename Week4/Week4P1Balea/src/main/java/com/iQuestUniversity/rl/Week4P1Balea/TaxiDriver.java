package com.iQuestUniversity.rl.Week4P1Balea;

import interfaces.ITaxiDriver;

public class TaxiDriver implements ITaxiDriver {

	private String name;
	
	public TaxiDriver(String name) {
		this.name = name;
	}
	public void eat() {
        System.out.println("Driver " + name + " eats");		
	}

	public String getName() {
		return name;
	}

	public void sleep() {
        System.out.println("Driver " + name + " sleeps");		
	}

	public String getCurrentLocation() {
        return "mock location";
	}

	public void goToAddress(String address) {
		System.out.println("Driver " + name + " goes to " + address);
	}

}
