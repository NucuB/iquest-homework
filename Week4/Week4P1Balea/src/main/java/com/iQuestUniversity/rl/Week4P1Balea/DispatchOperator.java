package com.iQuestUniversity.rl.Week4P1Balea;

import java.util.List;

import interfaces.ITaxiDispatcher;
import interfaces.ITaxiDriver;

public class DispatchOperator implements ITaxiDispatcher {
	
	private String name;
    private List<ITaxiDriver> drivers;
    
	public DispatchOperator(String name, List<ITaxiDriver> drivers) {
		this.name = name;
		this.drivers = drivers;
	}

	public void eat() {
		System.out.println(name + "(dispatcher) eats ");
	}

	public String getName() {
		return name;
	}

	private ITaxiDriver getBestAvailableTaxi(String location) {
		return drivers.get(0);
	}

	public void dispatch(String location) {
		 getBestAvailableTaxi(location).goToAddress(location);
		
	}

}
