package com.iQuestUniversity.rl.Week4P4Balea;

import interfaces.IGuitarist;

public class RhythmGuitarist implements IGuitarist {

	private Guitar guitar;

	public RhythmGuitarist(Guitar guitar) {
		this.guitar = guitar;
	}

	public void playGuitar() {
		System.out.println("Rhythm guitarist");

	}

}
