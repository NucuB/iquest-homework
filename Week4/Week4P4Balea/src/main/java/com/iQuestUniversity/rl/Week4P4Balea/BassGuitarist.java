package com.iQuestUniversity.rl.Week4P4Balea;

import interfaces.IGuitarist;

public class BassGuitarist implements IGuitarist {

	private Guitar guitar;

	public BassGuitarist(Guitar guitar) {
		this.guitar = guitar;
	}

	public void playGuitar() {
		System.out.println("Bass guitarist");
	}

}
