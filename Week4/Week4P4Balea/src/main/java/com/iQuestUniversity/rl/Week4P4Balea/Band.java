package com.iQuestUniversity.rl.Week4P4Balea;

import interfaces.IDrummer;
import interfaces.IGuitarist;

public class Band {

	private String name;
	private IDrummer drummer;
	private IGuitarist guitarist;

	public Band(String name, IDrummer drummer, IGuitarist guitarist) {
		this.name = name;
		this.drummer = drummer;
		this.guitarist = guitarist;
	}

	public void performShow() {
		System.out.println("Enjoy");
	}

}
