package com.iQuestUniversity.rl.Week4P4Balea;

import interfaces.IGuitarist;

public class LeadGuitarist implements IGuitarist {

	private Guitar guitar;

	public LeadGuitarist(Guitar guitar) {
		this.guitar = guitar;
	}

	public void playGuitar() {
		System.out.println("Lead guitarist");

	}

}
