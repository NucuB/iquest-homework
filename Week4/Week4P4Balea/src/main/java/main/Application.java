package main;

import com.iQuestUniversity.rl.Week4P4Balea.Band;
import com.iQuestUniversity.rl.Week4P4Balea.DrumSet;
import com.iQuestUniversity.rl.Week4P4Balea.Guitar;
import com.iQuestUniversity.rl.Week4P4Balea.LeadGuitarist;
import com.iQuestUniversity.rl.Week4P4Balea.MetalDrummer;

import music.CdPlayer;
import music.IPod;
import music.PlayList;
import music.Song;

public class Application {

	public static void main(String[] args) {

		Guitar guitar = new Guitar("Black");
		DrumSet drumSet = new DrumSet("blue");

		LeadGuitarist leadGuitarist = new LeadGuitarist(guitar);
		MetalDrummer metalDrummer = new MetalDrummer(drumSet);

		Band band = new Band("Heroes", metalDrummer, leadGuitarist);
		band.performShow();

		Song rap = new Song();
		Song pop = new Song();

		PlayList playList = new PlayList();
		playList.addSong(pop);
		playList.addSong(rap);

		CdPlayer cdPlayer = new CdPlayer(playList);
		IPod iPod = new IPod(playList);
		iPod.playMusic();
		cdPlayer.playMusic();

	}

}
