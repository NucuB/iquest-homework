package music;

import interfaces.IMusicPlayer;

public class IPod implements IMusicPlayer {
	
	private PlayList playList;
	
	public IPod(PlayList playList) {
		this.playList = playList;
	}
	
	public void playMusic() {

		System.out.println("Music from IPod");
	}

}
