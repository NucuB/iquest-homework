package music;

import java.util.ArrayList;
import java.util.List;

public class PlayList {

	private List<Song> songsList;
	
	public PlayList() {
		songsList = new ArrayList<Song>();
	}
	
	public void addSong(Song song) {
		songsList.add(song);
	}
}
