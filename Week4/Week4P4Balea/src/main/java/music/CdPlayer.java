package music;

import interfaces.IMusicPlayer;

public class CdPlayer implements IMusicPlayer {

	private PlayList playList;
	
	public CdPlayer(PlayList playList) {
		this.playList = playList;
	}
	public void playMusic() {
		
		System.out.println("Music from CdPlayer");
	}

}
