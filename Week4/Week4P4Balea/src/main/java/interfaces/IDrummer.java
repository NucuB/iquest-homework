package interfaces;

public interface IDrummer {

	public void playDrum();
}
