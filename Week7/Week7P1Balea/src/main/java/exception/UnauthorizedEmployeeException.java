package exception;
public class UnauthorizedEmployeeException extends RuntimeException {

    public UnauthorizedEmployeeException(String message) {
        super(message);
    }
    
}
