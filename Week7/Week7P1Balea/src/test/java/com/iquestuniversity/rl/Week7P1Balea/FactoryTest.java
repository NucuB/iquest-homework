package com.iquestuniversity.rl.Week7P1Balea;


import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import domain.Employee;
import domain.Engine;
import domain.EngineArchitecture;
import domain.EngineComponent;
import exception.InsufficientStockException;
import exception.UnauthorizedEmployeeException;
import exception.UnqualifiedEmployeeException;
import factory.EngineFactory;

public class FactoryTest {
	
	

	@Test(expected = UnqualifiedEmployeeException.class)
	public void testEmployeeNotQualified() {
		Employee employee = new Employee("name", true, false);
		EngineFactory engineFactory = new EngineFactory(Arrays.asList(employee), new ArrayList<EngineComponent>());
		engineFactory.manufactureEngines(2, employee);
	}

	@Test(expected = UnauthorizedEmployeeException.class)
	public void testEmployeeNotAuthorized() {
		Employee employee = new Employee("name", true, false);
		EngineFactory engineFactory = new EngineFactory(new ArrayList<Employee>(), new ArrayList<EngineComponent>());
		engineFactory.manufactureEngines(2, employee);
	}

	@Test(expected = InsufficientStockException.class)
	public void testInsufficientStock() {
		Employee employee = new Employee("name", false, true);
		EngineFactory engineFactory = new EngineFactory(Arrays.asList(employee),
				new ArrayList<EngineComponent>(Collections.nCopies(2, new EngineComponent("ec1", 10))));
		engineFactory.manufactureEngines(1, employee);
	}

	@Test
	public void testSufficientStock() {
		
		Employee employee = new Employee("name", false, true);
		EngineFactory engineFactory = new EngineFactory(Arrays.asList(employee),
				new ArrayList<EngineComponent>(Collections.nCopies(10, new EngineComponent("ec1", 10))));
		List<Engine> engines = engineFactory.manufactureEngines(2, employee);
		Assert.assertNotNull(engines);
		Assert.assertEquals(2, engines.size());
	}

	@Test
    public void testIfReturnedEnginesAreExpectedNumber() {
    	int expectedEngines = 3;
    	 
    	Employee employee = new Employee("name", false, true);
        EngineFactory engineFactory = new EngineFactory(Arrays.asList(employee), new ArrayList<EngineComponent>(Collections.nCopies(9, new EngineComponent("ec1", 10))));
        List<Engine> engines = engineFactory.manufactureEngines(expectedEngines, employee);
        Assert.assertNotNull(engines);
        Assert.assertEquals(expectedEngines, engines.size());
    }
	
	@Test
	public void testEngineArchitectureIsL4() {
		Employee employee = new Employee("name", false, true);
		EngineFactory engineFactory = new EngineFactory(Arrays.asList(employee),
				new ArrayList<EngineComponent>(Collections.nCopies(10, new EngineComponent("ec1", 10))));
		List<Engine> engines = engineFactory.manufactureEngines(1, employee);
		
		Assert.assertEquals(engines.get(0).getEngineArchitecture(), EngineArchitecture.L4);
		
	}
	
	@Test
	public void testEmployeeIsAdministrator() {
		Employee employee = new Employee("name", false, true);
		Assert.assertThat(employee.isAdministrator(), is(false));

	}
 
}
