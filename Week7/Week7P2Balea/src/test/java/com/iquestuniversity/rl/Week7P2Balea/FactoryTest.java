package com.iquestuniversity.rl.Week7P2Balea;



import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import domain.Employee;
import domain.Engine;
import exception.UnauthorizedEmployeeException;
import exception.UnqualifiedEmployeeException;
import factory.EngineFactory;
import service.EmployeeService;

import java.util.ArrayList;
import java.util.List;

public class FactoryTest {

    private EngineFactory engineFactory;
    private EmployeeService employeeService;

    @Before
    public void setupMock() {
        employeeService = EasyMock.createMock(EmployeeService.class);
        engineFactory = new EngineFactory(employeeService);
    }

    @Test(expected = UnqualifiedEmployeeException.class)
    public void testEmployeeNotQualified() {
        Employee employee = new Employee("name");
        EasyMock.expect(employeeService.isAssemblyLineWorker(employee)).andReturn(false);
        EasyMock.replay(employeeService);

        engineFactory.manufactureEngines(2, employee);

        EasyMock.verify(employeeService);
    }

    @Test
    public void testEmployeeQualified() {
        Employee employee = new Employee("name");
        EasyMock.expect(employeeService.isAssemblyLineWorker(employee)).andReturn(true);
        EasyMock.replay(employeeService);

        List<Engine> engines = engineFactory.manufactureEngines(2, employee);

        EasyMock.verify(employeeService);
        Assert.assertNotNull(engines);
        Assert.assertEquals(2, engines.size());
    }
    
 

}
