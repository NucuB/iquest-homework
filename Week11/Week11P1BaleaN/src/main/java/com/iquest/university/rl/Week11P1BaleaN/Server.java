package com.iquest.university.rl.Week11P1BaleaN;

import java.util.LinkedList;
import java.util.List;

public class Server extends Thread {

	private List<String> messages;
	private int size;
	private int counterProducer = 0;
	private int counterConsumer = 0;
	private boolean allowingMessages;

	public Server(int size) {
		allowingMessages = true;
		messages = new LinkedList<String>();
		this.size = size;
	}

	public synchronized void produceMessage() {

		while (messages.size() == size) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String message = "New message added in list";
		messages.add(message);
		System.out.println("Counter producer : " + counterProducer++ + " produced : " + message);
		notifyAll();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized void consumeMessage() {

		while (messages.size() == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String value = messages.remove(0);
		System.out.println("Value removed : [ " + value + " ]");
		System.out.println("Counter consumer : " + counterConsumer++);

		notifyAll();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized int getSize() {
		return size;
	}

	public synchronized List<String> getMessages() {
		return messages;
	}

	public void run() {
		while (allowingMessages) {
			consumeMessage();
		}
	}

	public boolean isAllowingMessages() {
		return allowingMessages;
	}

	public void setAllowingMessages(boolean allowingMessages) {
		this.allowingMessages = allowingMessages;
	}

}
