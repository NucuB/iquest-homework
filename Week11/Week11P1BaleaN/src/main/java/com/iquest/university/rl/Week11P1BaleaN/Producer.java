package com.iquest.university.rl.Week11P1BaleaN;

public class Producer extends Thread {
	private Server server;
	private boolean producingFlag;

	public Producer(Server server) {
		producingFlag = true;
		this.server = server;
	}

	public void run() {
		while (producingFlag) {
			server.produceMessage();
		}
	}

	public boolean isProducingFlag() {
		return producingFlag;
	}

	public void setProducingFlag(boolean producingFlag) {
		this.producingFlag = producingFlag;
	}

}
