package main;

import com.iquest.university.rl.Week11P1BaleaN.Producer;
import com.iquest.university.rl.Week11P1BaleaN.Server;

public class App {
	public static void main(String[] args) {
		Server server = new Server(10);

		Producer producerThread = new Producer(server);
		producerThread.start();
		Producer producerThread2 = new Producer(server);
		producerThread2.start();
		Producer producerThread3 = new Producer(server);
		producerThread3.start();

		server.start();

	}
}
