package com.iquest.university.rl.Week11P2Balea;

import java.util.concurrent.locks.Lock;

public class ThreadRace implements Runnable {

	private int circuitLaps;
	private ThreadRaceContext raceContext;
	private int numberOfCompetitors;
	private boolean allCompetitorsFinished;
	private Lock lock;

	public ThreadRace(int numberOfCompetitors, int circuitLaps, ThreadRaceContext raceContext, Lock lock) {
		allCompetitorsFinished = false;
		this.circuitLaps = circuitLaps;
		this.raceContext = raceContext;
		this.numberOfCompetitors = numberOfCompetitors;
		this.lock = lock;

	}

	private synchronized void generateCompetitors() {
		for (int number = 0; number < numberOfCompetitors; number++) {
			Thread threadCompetitor = new Thread(new ThreadRaceCompetitor(number, this, lock));
			threadCompetitor.start();
		}
	}

	public synchronized ThreadRaceContext getRaceContext() {
		return raceContext;
	}

	public synchronized int getLaps() {
		return circuitLaps;
	}

	public void run() {
		generateCompetitors();
		while (!allCompetitorsFinished) {
			if (raceContext.getScoreCard().size() == numberOfCompetitors) {
				allCompetitorsFinished = true;
			}
		}
	}

}
