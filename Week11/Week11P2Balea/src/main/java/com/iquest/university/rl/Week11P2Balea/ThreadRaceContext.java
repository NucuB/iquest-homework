package com.iquest.university.rl.Week11P2Balea;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

public class ThreadRaceContext implements Runnable {

	private List<String> scoreCard;
	private boolean raceFinished;
	private int currentSize;
	private int numberOfCompetitors;
	private Lock lock;

	public ThreadRaceContext(int numberOfCompetitors, Lock lock) {
		this.numberOfCompetitors = numberOfCompetitors;
		this.lock = lock;
		currentSize = 0;
		raceFinished = false;
		scoreCard = new ArrayList<String>();
	}

	public synchronized List<String> getScoreCard() {
		return scoreCard;
	}

	public synchronized void addRank(String string) {

		try {
			scoreCard.add(string);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	public void run() {
		while (!raceFinished) {
			lock.lock();
			try {
				if (currentSize != scoreCard.size()) {
					currentSize++;
				}
				if (currentSize == numberOfCompetitors) {
					raceFinished = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}

		}
		System.out.println("Final results : " + scoreCard);

	}

}
