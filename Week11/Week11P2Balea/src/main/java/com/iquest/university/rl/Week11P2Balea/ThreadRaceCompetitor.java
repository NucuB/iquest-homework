package com.iquest.university.rl.Week11P2Balea;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadRaceCompetitor implements Runnable {

	private int competitorId;
	private int currentLap;
	private ThreadRace race;
	private Lock lock;
	private boolean finishedRace;


	public ThreadRaceCompetitor(int id, ThreadRace race, Lock lock) {
		finishedRace = false;
		currentLap = 0;
		lock = new ReentrantLock();
		this.race = race;
		this.competitorId = id;
		this.lock = lock;

	}

	public void run() {
		while (!finishedRace) {
			startRunning();
		}
		informContext();

	}

	private void informContext() {
		lock.lock();
		try {
			race.getRaceContext().addRank("Thread competitor : " + competitorId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}

	}
	
	private synchronized void startRunning() {
		currentLap++;
		if (currentLap == race.getLaps()) {
			finishedRace = true;
		}
	}

	public synchronized int getCompetitorId() {
		return competitorId;
	}

}
