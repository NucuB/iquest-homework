package main;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.iquest.university.rl.Week11P2Balea.ThreadRace;
import com.iquest.university.rl.Week11P2Balea.ThreadRaceContext;

public class App {
	public static void main(String[] args) {
		Lock lock = new ReentrantLock();
		int numberOfCompetitors = 10;
		int circuitLaps = 5;

		ThreadRaceContext context = new ThreadRaceContext(numberOfCompetitors, lock);

		ThreadRace race = new ThreadRace(numberOfCompetitors, circuitLaps, context, lock);
		Thread threadRace = new Thread(race);
		threadRace.start();

		Thread threadContext = new Thread(context);
		threadContext.start();

	}
}
