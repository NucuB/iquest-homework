package com.iquest.university.rl.Week11P3Balea;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadRaceContext implements Runnable {

	private List<String> scoreCard;
	private Lock lock;
	private boolean raceFinished;
	private int currentSize;
	private int numberOfCompetitors;

	public ThreadRaceContext(Lock lock, int numberOfCompetitors) {
		scoreCard = new ArrayList<String>();
		currentSize = 0;
		this.lock = lock;
		this.numberOfCompetitors = numberOfCompetitors;
	}

	public synchronized List<String> getScoreCard() {
		return scoreCard;
	}

	public void addRank(String string) {
		lock.lock();
		try {
			scoreCard.add(string);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
	}

	public void run() {
		while (!raceFinished) {
			lock.lock();
			try {
				if (currentSize != scoreCard.size()) {
					currentSize++;
				}
				if (currentSize == numberOfCompetitors) {
					raceFinished = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}

		}
		System.out.println("Final results : " + scoreCard);

	}
}
