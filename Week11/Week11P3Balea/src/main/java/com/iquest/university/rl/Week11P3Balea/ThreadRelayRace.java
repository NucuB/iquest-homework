package com.iquest.university.rl.Week11P3Balea;

import java.util.concurrent.locks.Lock;

public class ThreadRelayRace implements Runnable{

	private int numberOfCompetitorsPerTeam;
	private final String[] enrolledTeams;
	private int numberOfTeams;
	private int circuitLaps;
	private ThreadRaceContext context;
	private Lock lock;
	private boolean allCompetitorsFinished;

	public ThreadRelayRace(int numberOfTeams, int numberOfCompetitorsPerTeam, String[] enrolledTeams, int circuitLaps, ThreadRaceContext context, Lock lock) {
		this.numberOfTeams = numberOfTeams;
		this.enrolledTeams = enrolledTeams;
		this.circuitLaps = circuitLaps;
		this.context = context;
		this.lock = lock;
		this.numberOfCompetitorsPerTeam = numberOfCompetitorsPerTeam;
	}

	public  void startRace() {
		for (int number = 0; number < numberOfTeams; number++) {
			ThreadRelayRaceTeam raceTeam = new ThreadRelayRaceTeam(numberOfCompetitorsPerTeam, enrolledTeams[number], this, lock);
			raceTeam.start();
		}

	}

	public synchronized String[] getEnrolledTeams() {
		return enrolledTeams;
	}

	public synchronized int getNumberOfTeams() {
		return numberOfTeams;
	}

	public synchronized int getCircuitLaps() {
		return circuitLaps;
	}

	public synchronized ThreadRaceContext getContext() {
		return context;
	}

	public void run() {
		startRace();
		while (!allCompetitorsFinished) {
			if (context.getScoreCard().size() == numberOfCompetitorsPerTeam * numberOfTeams) {
				allCompetitorsFinished = true;
			}
		}
		
	}

}
