package com.iquest.university.rl.Week11P3Balea;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadCompetitor extends Thread {

	private int id;
	private String teamName;
	private int currentLap;
	private ThreadRelayRaceTeam team;
	private Lock lock;
	private boolean finishedLaps;

	public ThreadCompetitor(int id, String teamName, ThreadRelayRaceTeam team, Lock lock) {
		this.id = id;
		this.teamName = teamName;
		this.team = team;
		this.lock = lock;
		currentLap = 0;
		finishedLaps = false;
	}

	public void run() {

		while (!finishedLaps && !team.isTeamHasRelay()) {
			
			startRunning();
		}
		
		informContext();

	}

	private void informContext() {
		lock.lock();

		try {
			team.getRace().getContext()
					.addRank("Thread competitor : " + id + " from team : " + team.getTeamName() + " now the next team mate start running \n");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}

	}
	
	private synchronized void startRunning() {
		team.setTeamHasRelay(true);

		while (currentLap < team.getRace().getCircuitLaps()) {
			currentLap++;
		}
		finishedLaps = true;
		team.setTeamHasRelay(false);
		
		/*if (currentLap == team.getRace().getCircuitLaps()) {
			finishedLaps = true;
		}
		if (finishedLaps) {
			team.setTeamHasRelay(false);

		}*/
	}

}
