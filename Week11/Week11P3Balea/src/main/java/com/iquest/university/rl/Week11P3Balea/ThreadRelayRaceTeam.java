package com.iquest.university.rl.Week11P3Balea;

import java.util.concurrent.locks.Lock;

public class ThreadRelayRaceTeam extends Thread {

	private int numberOfCompetitors;
	private String teamName;
	private Thread[] threadTeam;
	private ThreadRelayRace race;
	private boolean teamHasRelay;
	private Lock lock;


	public ThreadRelayRaceTeam(int numberOfCompetitors, String teamName, ThreadRelayRace race, Lock lock) {
		this.numberOfCompetitors = numberOfCompetitors;
		this.teamName = teamName;
		this.race = race;
		this.threadTeam = new Thread[this.numberOfCompetitors];
		this.lock = lock;
		teamHasRelay = false;
	}

	private  void prepareAndStartTeamCompetitors() {
		for (int number = 0; number < numberOfCompetitors; number++) {
			threadTeam[number] = new ThreadCompetitor(number, teamName, this, lock);
			threadTeam[number].start();
			try {
				threadTeam[number].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public  Thread[] getThreadTeam() {
		return threadTeam;
	}

	public synchronized int getNumberOfCompetitors() {
		return numberOfCompetitors;
	}

	public synchronized String getTeamName() {
		return teamName;
	}

	public synchronized ThreadRelayRace getRace() {
		return race;
	}

	public synchronized boolean isTeamHasRelay() {
		return teamHasRelay;
	}

	public synchronized void setTeamHasRelay(boolean teamHasRelay) {
		this.teamHasRelay = teamHasRelay;
	}

	public void run() {
		prepareAndStartTeamCompetitors();
	}
	
	

}
