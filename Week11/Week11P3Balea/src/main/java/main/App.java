package main;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.iquest.university.rl.Week11P3Balea.ThreadRaceContext;
import com.iquest.university.rl.Week11P3Balea.ThreadRelayRace;

public class App 
{
	public static final int MAX_COMPETITORS_PER_TEAM = 4;
    public static void main( String[] args )
    {
    	Lock lock = new ReentrantLock();
    	String[] enrolledTeams = {"A","B","C","D","E","F","G","H","I","J"};
    	int circuitLaps = 5;
    	
    	ThreadRaceContext context = new ThreadRaceContext(lock, MAX_COMPETITORS_PER_TEAM * enrolledTeams.length);
    	ThreadRelayRace race = new ThreadRelayRace(enrolledTeams.length, MAX_COMPETITORS_PER_TEAM, enrolledTeams, circuitLaps, context, lock);
    	Thread threadRace = new Thread(race);
    	threadRace.start();
    	Thread threadContext = new Thread(context);
    	threadContext.start();
    
    }
}
