package com.iquest.university.rl.Week11P1Balea;

import java.util.LinkedList;
import java.util.List;

public class Server {

	private List<String> messages;
	private int size;
	private int counterProducer = 0;
	private int counterConsumer = 0;
	private boolean allowingMessages;

	public Server(int size) {
		allowingMessages = true;
		messages = new LinkedList<String>();
		this.size = size;
	}

	public synchronized void produceMessage() {

		while (messages.size() == size) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String message = "New message added in list";
		messages.add(message);
		System.out.println("Counter producer : " + counterProducer++ + " produced : " + message);
		allowingMessages = true;

		notifyAll();

		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void consumeMessage() {

		while (messages.size() == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String value = messages.remove(0);
		System.out.println("Value removed : [ " + value + " ]");
		System.out.println("Counter consumer : " + counterConsumer++);
		if (messages.size() == 0) {
			allowingMessages = false;
		}

		notifyAll();
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized int getSize() {
		return size;
	}

	public synchronized List<String> getMessages() {
		return messages;
	}

	public boolean isAllowingMessages() {
		return allowingMessages;
	}

	public void setAllowingMessages(boolean allowingMessages) {
		this.allowingMessages = allowingMessages;
	}

}
