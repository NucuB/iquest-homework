package com.iquest.university.rl.Week11P1Balea;

public class Producer implements Runnable {
	private Server server;

	public Producer(Server server) {
		this.server = server;
	}

	public void run() {

		for (int counter = 0; counter < server.getSize(); counter++) {
			server.produceMessage();
		}
		

	}

}
