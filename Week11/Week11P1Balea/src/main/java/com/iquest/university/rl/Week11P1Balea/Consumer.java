package com.iquest.university.rl.Week11P1Balea;

public class Consumer implements Runnable {

	private Server server;

	public Consumer(Server server) {
		this.server = server;
	}

	public void run() {

		while (server.isAllowingMessages()) {
			server.consumeMessage();
			System.out.println("Consumer : " + server.isAllowingMessages());
			
		}
		
		
	}

}
