package main;

import com.iquest.university.rl.Week11P1Balea.Consumer;
import com.iquest.university.rl.Week11P1Balea.Producer;
import com.iquest.university.rl.Week11P1Balea.Server;

public class App {
	public static void main(String[] args) {
		
		int numberOfProducers = 3;
		int sizeOfServer = 10;
		Server server = new Server(sizeOfServer);
		
		Thread consumerThread = new Thread(new Consumer(server));
		consumerThread.start();
		
		for (int number = 0; number < numberOfProducers; number++) {
			Thread producerThread = new Thread(new Producer(server));
			producerThread.start();
		}

		try {
			consumerThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
