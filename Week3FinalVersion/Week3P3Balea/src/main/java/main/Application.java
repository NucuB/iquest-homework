package main;

import com.iquestuniversity.rl.Week3P3Balea.GraphicalEditor;

import shapes.Canvas;
import shapes.Circle;
import shapes.Line;
import shapes.Point;
import shapes.Rectangle;

public class Application {

	public static void main(String[] args) {

		Point leftPoint = new Point(0, 0);
		Point rightPoint = new Point(5, 5);

		Line line = new Line(leftPoint, rightPoint);

		Circle circle = new Circle(line);

		Rectangle rectangle = new Rectangle();

		Canvas mainCanvas = new Canvas();
		Canvas oilCanvas = new Canvas();
		
		mainCanvas.addShapeIntoCanvas(rightPoint);
		mainCanvas.addShapeIntoCanvas(line);
		mainCanvas.addShapeIntoCanvas(circle);
		mainCanvas.addShapeIntoCanvas(rectangle);
		mainCanvas.addShapeIntoCanvas(oilCanvas);

		rightPoint.fillWithColor("blue");
		line.fillWithColor("green");
		circle.fillWithColor("red");
		rectangle.fillWithColor("black");
		oilCanvas.fillWithColor("purple");

		GraphicalEditor editor = new GraphicalEditor(mainCanvas);
		editor.getCanvas().printColorsOfShapesFromCanvas();

	}

}
