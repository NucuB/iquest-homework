package shapes;

public class Line extends Shape {

	private Point leftPoint;
	private Point rightPoint;
	private String color;

	public Line(Point leftPoint, Point rightPoint) {
		this.rightPoint = rightPoint;
		this.leftPoint = leftPoint;
		this.getPointsList().add(leftPoint);
		this.getPointsList().add(rightPoint);
	}

	@Override
	public void fillWithColor(String color) {

		this.color = color;
	}

	@Override
	public String getColor() {
		System.out.println("Color of line is : ");
		return color;
	}

	

}
