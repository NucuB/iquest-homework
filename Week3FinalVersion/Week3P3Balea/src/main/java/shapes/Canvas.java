package shapes;

import java.util.ArrayList;

public class Canvas extends Shape {

	private ArrayList<Shape> shapeList;
	private String color;

	public Canvas() {
		shapeList = new ArrayList<Shape>();
	}

	public void addShapeIntoCanvas(Shape shape) {
		shapeList.add(shape);
	}

	public void printColorsOfShapesFromCanvas() {
		for (Shape shape : shapeList) {
			System.out.println(shape.getColor());
		}
	}

	public ArrayList<Shape> getShapeList() {
		return shapeList;
	}

	@Override
	public void fillWithColor(String color) {
		this.color = color;

	}

	@Override
	public String getColor() {
		System.out.println("Color of canvas is : ");
		return color;
	}
}
