package shapes;

import java.util.ArrayList;

public abstract class Shape {

	private ArrayList<Point> pointsList = new ArrayList<Point>();

	public abstract void fillWithColor(String color);

	public abstract String getColor();

	public ArrayList<Point> getPointsList() {
		return pointsList;
	}

}
