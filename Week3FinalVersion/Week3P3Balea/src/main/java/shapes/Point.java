package shapes;

public class Point extends Shape {

	private int xCoordinate;
	private int yCoordinate;
	private String color;

	public Point(int xCoordinate, int yCoordinate) {
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
	}

	public int getxCoordinate() {
		return xCoordinate;
	}

	@Override
	public String getColor() {
		System.out.println("Color of point is : ");
		return color;
	}

	public int getyCoordinate() {
		return yCoordinate;
	}

	@Override
	public void fillWithColor(String color) {
		this.color = color;
	}

}
