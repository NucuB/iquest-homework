package shapes;

public class Rectangle extends Shape {

	private final static int MAX_POINTS = 4;
	private final static int MAX_LINES = 4;

	private Line[] lineArary = new Line[MAX_LINES];
	private String color;

	public Rectangle() {
		for (int index = 0; index < MAX_POINTS; index++) {
			Point point = new Point(index, index);
			this.getPointsList().add(point);
		}

		for (int index = 0; index < MAX_LINES; index++) {
			lineArary[index] = new Line(this.getPointsList().get(index), this.getPointsList().get(index));
		}

	}

	@Override
	public void fillWithColor(String color) {
		this.color = color;

	}

	public String getColor() {
		System.out.println("Color of rectangle is : ");
		return color;
	}

}
