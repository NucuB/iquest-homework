package shapes;

public class Circle extends Shape {

	private final static int NUMBER_OF_POINTS = 10;
	private String color;
	private Line radius;

	public Circle(Line radius) {
		for (int index = 0; index < NUMBER_OF_POINTS; index++) {
			Point point = new Point(index, index);
			this.getPointsList().add(point);
		}
		this.radius = radius;
	}

	@Override
	public void fillWithColor(String color) {

		this.color = color;
	}

	@Override
	public String getColor() {
		System.out.println("Color of circle is : ");
		return color;
	}

}
