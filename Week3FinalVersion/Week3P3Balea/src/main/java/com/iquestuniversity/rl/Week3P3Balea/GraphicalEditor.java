package com.iquestuniversity.rl.Week3P3Balea;

import shapes.Canvas;

public class GraphicalEditor {

	private Canvas canvas;

	public GraphicalEditor(Canvas canvas) {
		this.canvas = canvas;
	}

	public Canvas getCanvas() {
		return canvas;
	}

}
