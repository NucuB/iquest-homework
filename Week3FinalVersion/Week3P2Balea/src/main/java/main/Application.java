package main;

import com.iquestuniversity.rl.Week3P2Balea.Amphibian;
import com.iquestuniversity.rl.Week3P2Balea.Frog;

public class Application {

	public static void main(String[] args) {

		Frog frog = new Frog("Kermit");
		Amphibian amphibian = (Amphibian) frog;

		System.out.println("Name of amphibian : " + amphibian.getName());

		amphibian.setName("Ninja");
		System.out.println("Name of amphibian after using setName method : " + amphibian.getName());
		amphibian.jump();

		frog.jump();
		System.out.println("Name of frog : " + frog.getName());

	}

}
