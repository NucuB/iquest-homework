package com.iquestuniversity.rl.Week3P2Balea;

public class Amphibian {

	private String name;

	public Amphibian(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void jump() {
		System.out.println("Jumped");
	}

}
