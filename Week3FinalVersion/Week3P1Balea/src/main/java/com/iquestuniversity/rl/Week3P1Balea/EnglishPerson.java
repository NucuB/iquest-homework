package com.iquestuniversity.rl.Week3P1Balea;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class EnglishPerson extends Person {

	public EnglishPerson(String fullName, String birthDate) {
		super(fullName);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		this.setBirthDate(LocalDate.parse(birthDate, formatter));

	}

	public EnglishPerson(String firstName, String surName, String birthDate) {
		super(firstName, surName);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		this.setBirthDate(LocalDate.parse(birthDate, formatter));

	}

	@Override
	public int getBirthDate() {
		int age = this.calculateAge();
		return age;
	}

	@Override
	public String selfDescribe() {
		return "Person with surname : " + getSurName() + " and firstname : " + getFirstName() + " has age : " + getBirthDate();
	}

	
}
