package com.iquestuniversity.rl.Week3P1Balea;

import java.time.LocalDate;
import java.time.Period;

public abstract class Person {

	private String surName;
	private String firstName;
	private LocalDate birthDate;

	public Person(String firstName, String surName) {
		this.firstName = firstName;
		this.surName = surName;
	}

	public Person(String fullName) {

		String[] stringAfterSplitName = fullName.split(" ");
		surName = stringAfterSplitName[stringAfterSplitName.length - 1];
		firstName = extractFirstName(fullName);

	}

	public abstract int getBirthDate();

	public abstract String selfDescribe();

	public int calculateAge() {
		LocalDate currentDate = LocalDate.now();
		return Period.between(birthDate, currentDate).getYears();
	}
	
	
	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {

		return "Person's Firstname(s) : " + firstName + " and Surname : " + surName;
	}

	private String extractFirstName(String fullName) {
		String[] stringAfterSplitName = fullName.split(" ");
		StringBuilder stringBuilder = new StringBuilder();
		for (int index = 0; index <= stringAfterSplitName.length - 2; index++) {
			stringBuilder.append(stringAfterSplitName[index]);
			stringBuilder.append(" ");
		}
		return stringBuilder.toString();
	}

}
