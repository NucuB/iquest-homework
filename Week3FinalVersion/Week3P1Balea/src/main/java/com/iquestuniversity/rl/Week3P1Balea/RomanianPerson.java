package com.iquestuniversity.rl.Week3P1Balea;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class RomanianPerson extends Person {


	public RomanianPerson(String fullName, String birthDate) {
		super(fullName);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		this.setBirthDate(LocalDate.parse(birthDate, formatter));

	}

	public RomanianPerson(String firstName, String surName, String birthDate) {
		super(firstName, surName);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		this.setBirthDate(LocalDate.parse(birthDate, formatter));

	}

	@Override
	public int getBirthDate() {
		int age = calculateAge();
		return age;
	}

	@Override
	public String selfDescribe() {
		return "Persoana cu numele de familie : " + getSurName() + " si cu prenumele : " + getFirstName() + " are varsta de : "
				+ getBirthDate();
	}

	

}
