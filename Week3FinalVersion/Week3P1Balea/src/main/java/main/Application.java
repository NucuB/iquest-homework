package main;

import com.iquestuniversity.rl.Week3P1Balea.EnglishPerson;
import com.iquestuniversity.rl.Week3P1Balea.Person;
import com.iquestuniversity.rl.Week3P1Balea.RomanianPerson;

public class Application {

	public static void main(String[] args) {

		Person romanianPerson = new RomanianPerson("Alexandru Ionut Popescu", "25-11-1997");
		Person englishPerson = new EnglishPerson("Kevin John", "Heart", "03/10/1995");

		System.out.println(romanianPerson.selfDescribe());
		System.out.println(englishPerson.selfDescribe());

	}

}
