package com.iquest.university.rl.Week12P3Balea;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class DecoderTest {

	private Decoder decoder;
	private Decoder reverseDecoder;
	private final String srcEncoding = "ISO-8859-1";
	private final String resultEncoding = "UTF-8";
	private final String inputFile = "inputLatin.txt";
	private final String destinationFile = "outputUTF.txt";

	@Before
	public void setUp() {
		try {
			decoder = new Decoder(inputFile, destinationFile, srcEncoding, resultEncoding);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDestinationFileSizeShouldBeNotNull() {
		try {
			decoder.decode();
		} catch (IOException e) {
			e.printStackTrace();
		}
		long destinationFileSize = decoder.getDestinationFileSize();
		assertNotEquals(0, destinationFileSize);
	}

	@Test
	public void testReverseDecoderOuputFileSizeShouldBeEqualToDestinationFileSize() {
		String inputFile = "inputUTF.txt";
		String destinationFile = "outputLatin1.txt";
		try {
			reverseDecoder = new Decoder(inputFile, destinationFile, resultEncoding, srcEncoding);
			reverseDecoder.decode();
		} catch (IOException e) {
			e.printStackTrace();
		}
		long inputFileSize = reverseDecoder.getInputFileSize();
		long outputFileSize = reverseDecoder.getDestinationFileSize();
		assertEquals(inputFileSize, outputFileSize);
	}

}
