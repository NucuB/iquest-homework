package main;

import java.io.IOException;

import com.iquest.university.rl.Week12P3Balea.Decoder;

public class App {
	public static void main(String[] args) throws IOException {
		String srcEncoding = "ISO-8859-1";
		String resultEncoding = "UTF-8";
		String inputFile = "inputLatin.txt";
		String destinationFile = "outputUTF.txt";

		Decoder decoder = new Decoder(inputFile, destinationFile, srcEncoding, resultEncoding);
		decoder.decode();
		decoder.displayDestinationFile();

		inputFile = "inputUTF.txt";
		destinationFile = "outputLatin1.txt";

		Decoder reverseDecoder = new Decoder(inputFile, destinationFile, resultEncoding, srcEncoding);
		reverseDecoder.decode();
		reverseDecoder.displayDestinationFile();
	}
}
