package com.iquest.university.rl.Week12P3Balea;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class Decoder {

	private Reader reader;
	private Writer writer;
	private String destinationFile;
	private String pathToInputFile;

	public Decoder(String inputFile, String destinationFile, String srcEncoding, String resultEncoding)
			throws IOException {
		pathToInputFile = getPathOfSpecifiedResource(inputFile);
		this.destinationFile = getPathOfSpecifiedResource(destinationFile);
		reader = new BufferedReader(new InputStreamReader(new FileInputStream(pathToInputFile), srcEncoding));
		writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.destinationFile), resultEncoding));
	}

	public void decode() throws IOException {
		try {
			char[] buffer = new char[16384];
			int read;
			while ((read = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, read);
			}

		} finally {
			reader.close();
			writer.close();
		}

	}

	public void displayDestinationFile() throws IOException {
		BufferedReader destinationFileReader = new BufferedReader(
				new InputStreamReader(new FileInputStream(destinationFile)));
		String text;
		while ((text = destinationFileReader.readLine()) != null) {
			System.out.println(text);

		}
		destinationFileReader.close();
	}

	public long getDestinationFileSize() {
		File outputFile = new File(destinationFile);
		return outputFile.length();
	}

	public long getInputFileSize() {
		File inputFile = new File(pathToInputFile);
		return inputFile.length();
	}

	private String getPathOfSpecifiedResource(String fileName) {
		return this.getClass().getClassLoader().getResource(fileName).getPath();
	}
}
