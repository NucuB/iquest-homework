package com.iquest.university.rl.Week12P1Balea;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FileReaderWriterTest {

	private FileReader fileReader;
	private FileWriter fileWriter;
	private String file;

	@Before
	public void setUp() {
		file = "file.txt";
		fileWriter = new FileWriter(file);
	}

	@Test
	public void testFileWriter() {
		int totalNumbersWritten = 4;
		for (int number = 1; number <= totalNumbersWritten; number++) {
			fileWriter.writeIntegerInFile(number);
		}
		long expectedLengthInBytes = totalNumbersWritten * Integer.BYTES;
		long actualLengthInBytes = fileWriter.getFileSize();
		assertEquals(expectedLengthInBytes, actualLengthInBytes);
		fileWriter.closeFileWriter();
	}
	
	@Test
	public void testFileReader() {
		fileReader = new FileReader(file);
		int actualResult = fileReader.readUsingRandomAccess(2);
		int expectedResult = 3;
		assertEquals(expectedResult, actualResult);
		fileReader.closeFileReader();
	}

	
}
