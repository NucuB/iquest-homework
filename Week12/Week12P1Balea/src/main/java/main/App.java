package main;

import java.io.IOException;

import com.iquest.university.rl.Week12P1Balea.FileReader;
import com.iquest.university.rl.Week12P1Balea.FileWriter;

public class App {
	public static void main(String[] args) throws IOException {
		String fileWritten = "file.txt";
		int totalNumbersWritten = 4;

		FileWriter fileWriter = new FileWriter(fileWritten);
		for (int number = 1; number <= totalNumbersWritten; number++) {
			fileWriter.writeIntegerInFile(number);

		}
		fileWriter.closeFileWriter();

		FileReader fileReader = new FileReader(fileWritten);
		fileReader.readUsingRandomAccess(2);
		fileReader.readUsingRandomAccess(3);
		fileReader.closeFileReader();

	}
}
