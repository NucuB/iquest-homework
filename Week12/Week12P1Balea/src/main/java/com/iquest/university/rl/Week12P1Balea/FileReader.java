package com.iquest.university.rl.Week12P1Balea;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class FileReader {

	private RandomAccessFile file;

	public FileReader(String file) {
		String fileToBeReadPath = getPathToSpecifiedResource(file);

		try {
			this.file = new RandomAccessFile(fileToBeReadPath, "r");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public int readUsingRandomAccess(int indexOfElement) {
		int numberReadFromFile = 0;
		try {
			file.seek(indexOfElement * Integer.BYTES);
			numberReadFromFile = file.readInt();
			System.out.println("Element with specified index : " + numberReadFromFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return numberReadFromFile;
	}

	public long getSize() {
		try {
			return file.length();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void closeFileReader() {
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String getPathToSpecifiedResource(String fileName) {
		return this.getClass().getClassLoader().getResource(fileName).getPath();

	}

}
