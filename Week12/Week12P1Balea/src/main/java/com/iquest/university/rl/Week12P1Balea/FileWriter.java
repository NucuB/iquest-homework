package com.iquest.university.rl.Week12P1Balea;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class FileWriter {

	private RandomAccessFile file;

	public FileWriter(String file) {
		String fileToBeWrittenPath = getPathToSpecifiedResource(file);
		try {
			this.file = new RandomAccessFile(fileToBeWrittenPath, "rw");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void writeIntegerInFile(int number) {
		try {
			file.writeInt(number);
			System.out.println("Number written is : " + number);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void closeFileWriter() {
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public long getFileSize() {
		try {
			return file.length();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	private String getPathToSpecifiedResource(String fileName) {
		return this.getClass().getClassLoader().getResource(fileName).getPath();

	}

}
