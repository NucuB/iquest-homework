package com.iquest.university.rl.Week12P2Balea;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class FileCompresser {

	private Reader reader;
	private BufferedOutputStream writer;
	private String pathToInputFile;
	private String pathToDestinationFile;

	public FileCompresser(String file, String fileDestionation) throws IOException {
		this.pathToInputFile = getPathOfSpecifiedResource(file);
		this.pathToDestinationFile = fileDestionation;
		reader = new BufferedReader(new FileReader(pathToInputFile));
		writer = new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(fileDestionation)));
	}

	public void compressInputFile() throws IOException {
		int elementFromStream = 0;
		while ((elementFromStream = reader.read()) != -1) {
			writer.write(elementFromStream);
		}
		closeCompresser();
	}

	public void displayDestinationFile() throws IOException {
		BufferedReader destinationFileReader = new BufferedReader(
				new InputStreamReader(new GZIPInputStream(new FileInputStream(pathToDestinationFile))));
		String text;
		while ((text = destinationFileReader.readLine()) != null) {
			System.out.println(text);
		}
		destinationFileReader.close();
	}

	public double getCompressionRatioInBytes() {
		double initialFileLength = (double) getSizeOfInputFile();
		double compressedFileLength = (double) getSizeOfCompressedFile();
		return initialFileLength / compressedFileLength;
	}

	public long getSizeOfInputFile() {
		return new File(pathToInputFile).length();
	}

	public long getSizeOfCompressedFile() {
		return new File(pathToDestinationFile).length();
	}

	private void closeCompresser() throws IOException {
		reader.close();
		writer.close();
	}
	
	private String getPathOfSpecifiedResource(String fileName) {
		return this.getClass().getClassLoader().getResource(fileName).getPath();
	}
}
