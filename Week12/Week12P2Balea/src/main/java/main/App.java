package main;

import java.io.IOException;

import com.iquest.university.rl.Week12P2Balea.FileCompresser;

public class App 
{
    public static void main( String[] args ) throws IOException
    {
        String fileToBeCompressed = "file.txt";
        String fileCompressedDestination = "destination.gz";
        FileCompresser compresser = new FileCompresser(fileToBeCompressed, fileCompressedDestination);
        compresser.compressInputFile();
        compresser.displayDestinationFile();
        System.out.println(compresser.getCompressionRatioInBytes() + " bytes");
    }
}
