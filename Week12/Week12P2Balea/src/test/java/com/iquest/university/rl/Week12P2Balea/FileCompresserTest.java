package com.iquest.university.rl.Week12P2Balea;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class FileCompresserTest {

	private FileCompresser compresser;

	@Before
	public void setUp() {
		String fileToBeCompressed = "file.txt";
		String fileCompressedDestination = "destination.gz";
		try {
			compresser = new FileCompresser(fileToBeCompressed, fileCompressedDestination);
			compresser.compressInputFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testCompressedRatioShouldNotBeNull() {
		double actualResult = compresser.getCompressionRatioInBytes();
		assertNotEquals(0, actualResult);
	}

	@Test
	public void testCompressedFileShouldBeDifferentThanInputFile() {
		long inputFileSize = compresser.getSizeOfInputFile();
		long compressedFileSize = compresser.getSizeOfCompressedFile();
		assertNotEquals(inputFileSize, compressedFileSize);
	}
}
