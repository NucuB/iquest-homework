package com.iquest.university.rl.Week9P1Balea;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MyCollectionImplTest {

	private IMyCollection<String> myStringCollection;
	private IMyCollection<Integer> myIntegerCollection;
	@Before
	public void setUp() {
		myStringCollection = new MyCollectionImplementation<String>(10);
		myIntegerCollection = new MyCollectionImplementation<Integer>(20);

	}

	@Test
	public void testMyCollectionContainsAllMethod() {
		List<String> comparedCollection = new LinkedList<String>();
		comparedCollection.add("yes");
		comparedCollection.add("no");
		comparedCollection.add("right");
		// WHEN
		myStringCollection.add("yes");
		myStringCollection.add("no");
		myStringCollection.add("right");
		myStringCollection.add("test");

		boolean actualResult = myStringCollection.containsAll(comparedCollection);

		// THEN
		boolean expectedResult = true;
		assertThat(actualResult, is((expectedResult)));

	}

	@Test
	public void testMyCollectionAddAllMethod() {
		List<String> collectionToBeAdded = new LinkedList<String>();
		collectionToBeAdded.add("a");
		collectionToBeAdded.add("b");
		collectionToBeAdded.add("c");

		// WHEN
		boolean actualResult = myStringCollection.addAll(collectionToBeAdded);

		// THEN
		assertThat(actualResult, is((true)));

	}

	@Test
	public void testMyCollectionContainsAllMethodWithIntegers() {
		List<Integer> simpleIntegerCollection = new ArrayList<Integer>();
		simpleIntegerCollection.add(10);
		simpleIntegerCollection.add(20);
		simpleIntegerCollection.add(30);

		myIntegerCollection.add(10);
		myIntegerCollection.add(20);
		myIntegerCollection.add(30);
		myIntegerCollection.add(500);
		// WHEN
		boolean actualResult = myIntegerCollection.containsAll(simpleIntegerCollection);

		// THEN
		boolean expectedResult = true;
		assertThat(actualResult, is((expectedResult)));
	}

	@Test
	public void testMyCollectionAddAllMethodWithIntegers() {
		List<Integer> simpleIntegerCollection = new ArrayList<Integer>();
		simpleIntegerCollection.add(10);
		simpleIntegerCollection.add(20);
		simpleIntegerCollection.add(30);

		

		// WHEN
		boolean actualResult = myIntegerCollection.addAll(simpleIntegerCollection);

		// THEN
		assertThat(actualResult, is((true)));

	}

}
