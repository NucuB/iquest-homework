package com.iquest.university.rl.Week9P1Balea;

import java.util.Collection;

public interface IMyCollection<E> {

	boolean containsAll(Collection<E> c);
	boolean addAll(Collection<E> c);
	void add(E value);
}
