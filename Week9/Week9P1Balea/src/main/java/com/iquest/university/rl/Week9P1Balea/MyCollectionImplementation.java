package com.iquest.university.rl.Week9P1Balea;

import java.util.Collection;

public class MyCollectionImplementation<E> implements IMyCollection<E> {

	private E[] array;
	private int maximumSize;
	private int currentSize;

	public MyCollectionImplementation(int size) {
		maximumSize = size;
		currentSize = 0;
		array = ((E[]) new Object[maximumSize]);
	}

	public boolean containsAll(Collection<E> c) {
		int numberOfCollectionElements = c.size();
		int numbersFounded = 0;

		for (E iterator : c) {
			boolean found = false;
			for (int index = 0; index < currentSize; index++) {
				if (array[index] == iterator) {
					found = true;
				}
			}
			if (found) {
				numbersFounded++;
			}
		}
		if (numbersFounded == numberOfCollectionElements) {
			return true;
		}
		return false;
	}

	public boolean addAll(Collection<E> c) {
		int index = 0;
		int sizeBeforeAdd = currentSize;
		for (E iterator : c) {
			array[index++] = iterator;
			currentSize++;
		}
		if (currentSize != sizeBeforeAdd) {
			return true;
		}
		return false;
	}

	public void add(E value) {
		array[currentSize++] = value;
	}

}
