package com.iquest.university.rl.Week9P3Balea;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import exceptions.EmptyQueueException;
import exceptions.MaximumSizeException;
import exceptions.NullValueException;

public class CustomPriorityQueueTest {

	private CustomPriorityQueue<Integer> priorityQueue;

	@Before
	public void setUp() {
		priorityQueue = new CustomPriorityQueue<Integer>(3);
	}

	@Test(expected = NullValueException.class)
	public void testInsertNullValueShouldThrowNullException() {
		priorityQueue.insert(null);
	}

	@Test(expected = MaximumSizeException.class)
	public void testInsertOnFullSizeShouldThrowSizeException() {
		priorityQueue.insert(1);
		priorityQueue.insert(2);
		priorityQueue.insert(3);
		priorityQueue.insert(4);

	}
	
	@Test(expected = EmptyQueueException.class)
	public void testRemoveHeadFromEmptyQueueShouldThrowEmptyException() {
		priorityQueue.remove();
	}
	

	@Test
	public void testGetHeadShouldReturnLargestValue() {
		priorityQueue.insert(10);
		priorityQueue.insert(5);
		priorityQueue.insert(100);

		// WHEN
		int expectedResult = 100;

		// THEN
		assertThat(priorityQueue.head(), is(expectedResult));

	}

	@Test
	public void testRemoveHead() {
		priorityQueue.insert(10);
		priorityQueue.insert(5);
		priorityQueue.insert(100);
		priorityQueue.remove();
		assertEquals(priorityQueue.getSize(), 2);
	}

	@Test
	public void checkPriorityQueueIsEmpty() {
		assertThat(priorityQueue.isEmpty(), is(true));
	}
	
	@Test
	public void testClearShouldRemoveAllElements() {
		priorityQueue.insert(1);
		priorityQueue.insert(2);
		priorityQueue.insert(3);
		priorityQueue.clear();
		assertEquals(priorityQueue.getSize(), 0);
	}

	@Test
	public void testSortingGivenList() {
		List<Integer> toBeSortedList = new ArrayList<Integer>();
		toBeSortedList.add(10);
		toBeSortedList.add(100);
		toBeSortedList.add(50);

		List<Integer> expectedSortedResult = new ArrayList<Integer>();
		expectedSortedResult.add(100);
		expectedSortedResult.add(50);
		expectedSortedResult.add(10);

		List<Integer> returnedListFromPriorityQueue = priorityQueue.sort(toBeSortedList);
		assertThat(expectedSortedResult.toString(), is(returnedListFromPriorityQueue.toString()));

	}

	@Test
	public void testIfTwoQueuesAreEqual() {
		CustomPriorityQueue<String> firstQueue = new CustomPriorityQueue<String>(15);
		firstQueue.insert("a");
		firstQueue.insert("b");
		firstQueue.insert("c");

		CustomPriorityQueue<String> secondQueue = new CustomPriorityQueue<String>(20);
		secondQueue.insert("d");
		secondQueue.insert("e");
		secondQueue.insert("f");

		assertEquals(firstQueue, secondQueue);
	}
}
