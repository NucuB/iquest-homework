package com.iquest.university.rl.Week9P3Balea;

public interface IHeap<E extends Comparable<E>> {

	void insert(E element);
	E remove();
	void clear();
	E head();
	boolean isEmpty();
	int getSize();
}
