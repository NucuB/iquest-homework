package com.iquest.university.rl.Week9P3Balea;

import java.util.ArrayList;
import java.util.List;

import exceptions.EmptyQueueException;
import exceptions.MaximumSizeException;
import exceptions.NullValueException;

public class CustomPriorityQueue<E extends Comparable<E>> implements IHeap<E> {

	private E[] heap;
	private int currentSize;
	private int defaultSize = 10000;

	public CustomPriorityQueue() {
		currentSize = 0;
		heap = (E[]) new Comparable[defaultSize];
	}

	public CustomPriorityQueue(int newDefaultSize) {
		currentSize = 0;
		defaultSize = newDefaultSize;
		heap = (E[]) new Comparable[defaultSize];
	}

	public void insert(E element) {
		if (element == null) {
			throw new NullValueException("Null value has been inserted");
		}
		if (currentSize == defaultSize) {
			throw new MaximumSizeException("Priority Queue has reached its maximum size");
		}
		heap[currentSize] = element;
		traverseUp(currentSize++);
		

	}

	public E remove() {
		if (isEmpty()) {
			throw new EmptyQueueException("Queue is empty,no element can be removed");
		}
		E removedElement = heap[0];
		heap[0] = heap[currentSize - 1];
		heap[currentSize - 1] = null;
		currentSize--;
		maxHeapify(0);
		return removedElement;
	}

	public void clear() {
		while (currentSize != 0) {
			remove();
		}
	}

	public E head() {
		if (isEmpty()) {
			throw new EmptyQueueException("Queue is empty,no element can be retrieved");
		}
		return heap[0];
	}

	public boolean isEmpty() {
		if (currentSize == 0) {
			return true;
		}
		return false;
	}

	public int getSize() {
		return currentSize;
	}

	public List<E> sort(List<E> toBeSortedList) {
		List<E> toBeReturnedSortedList = (List<E>) new ArrayList<String>();

		for (E iterator : toBeSortedList) {
			insert(iterator);
		}
		while (currentSize != 0) {
			toBeReturnedSortedList.add(this.remove());
		}
		return toBeReturnedSortedList;
	}

	private void maxHeapify(int parent) {
		int left = (2 * parent) + 1;
		int right = (2 * parent) + 2;

		if (left > currentSize - 1)
			return;

		if (heap[right] == null || heap[left].compareTo(heap[right]) >= 0) {
			if (heap[left].compareTo(heap[parent]) > 0) {
				swap(parent, left);
				maxHeapify(left);
			}
		} else if (heap[right].compareTo(heap[parent]) > 0) {
			swap(parent, right);
			maxHeapify(right);
		}
	}

	private void traverseUp(int child) {
		if (child == 0)
			return;

		int parent = (child - 1) / 2;

		if (heap[child].compareTo(heap[parent]) > 0) {
			swap(parent, child);
			traverseUp(parent);
		}
	}

	private void swap(int parent, int child) {
		E temp = heap[parent];
		heap[parent] = heap[child];
		heap[child] = temp;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!(obj instanceof CustomPriorityQueue)) {
			return false;
		}

		CustomPriorityQueue<E> castedObject = (CustomPriorityQueue<E>) obj;

		if (getSize() == castedObject.getSize()) {
			return true;
		}

		return false;
	}

}
