package com.iquest.university.rl.Week9P2Balea;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class SorterTest {

	private Sorter sorter;

	@Before
	public void setUp() {
		sorter = new Sorter();

	}

	@Test
	public void testBubbleSortOnIntegers() {
		List<Integer> sortedCollection = new ArrayList<Integer>();
		sortedCollection.add(10);
		sortedCollection.add(20);
		sortedCollection.add(50);

		List<Integer> unsortedCollection = new ArrayList<Integer>();
		unsortedCollection.add(50);
		unsortedCollection.add(10);
		unsortedCollection.add(20);
		sorter.performBubbleSort(unsortedCollection);
		// WHEN
		String actualResult = unsortedCollection.toString();

		// THEN
		String expectedResult = sortedCollection.toString();
		assertThat(actualResult, is((expectedResult)));

	}

	@Test
	public void testBubbleSortOnStrings() {
		List<String> sortedCollection = new LinkedList<String>();
		sortedCollection.add("a");
		sortedCollection.add("b");
		sortedCollection.add("c");

		List<String> unsortedCollection = new ArrayList<String>();
		unsortedCollection.add("c");
		unsortedCollection.add("b");
		unsortedCollection.add("a");
		sorter.performBubbleSort(unsortedCollection);
		// WHEN
		String actualResult = unsortedCollection.toString();
		// THEN
		String expectedResult = sortedCollection.toString();

		assertThat(actualResult, is((expectedResult)));

	}
}
