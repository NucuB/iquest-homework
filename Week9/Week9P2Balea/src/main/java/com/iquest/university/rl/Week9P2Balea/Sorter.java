package com.iquest.university.rl.Week9P2Balea;

import java.util.List;

public class Sorter {

	public <T extends Comparable<T>> void performBubbleSort(List<T> collection) {
		int size = collection.size();
		for (int i = 0; i < size - 1; i++) {
			for (int j = 0; j < size - i - 1; j++) {
				if (((Comparable<T>) collection.get(j)).compareTo(collection.get(j + 1)) > 0) {
					swap(collection, j, j + 1);
				}
			}
		}
	}

	private <T> void swap(List<T> list, int first, int second) {
		T temp = list.get(first);
		list.set(first, list.get(second));
		list.set(second, temp);
	}

}
