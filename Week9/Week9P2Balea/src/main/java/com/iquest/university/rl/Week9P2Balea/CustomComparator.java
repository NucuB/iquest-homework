package com.iquest.university.rl.Week9P2Balea;

import java.util.Comparator;

public class CustomComparator<T extends Comparable<T>> implements Comparator<T>  {

	public int compare(T o1, T o2) {
		return o1.compareTo(o2);
	}

}
