# README #

## Java Learning Program ##

The purpose of the training program was to cover Java core, Design Patterns, OOP. The training program lasted 4 months, meaning 15 training sessions:

* Problem-solving(implementing algorithms, data structures)
* Various mini-applications(Datepicker, File Compresser, Pig Latin Translator, etc)
* Java 8
* JUnit(Test Driven Development)
* Concurrent Programming