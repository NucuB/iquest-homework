package com.iquest.university.rl.Week8P4Balea;

public class Train {

	private int trainNumber;
	private int nrWagons;
	private String type;
	private static final int hashCodeConstant = 3;

	public Train(int id, int nrWagons, String type) {
		trainNumber = id;
		this.nrWagons = nrWagons;
		this.type = type;
	}

	public int getTrainNumber() {
		return trainNumber;
	}

	public int getNrWagons() {
		return nrWagons;
	}

	public String getType() {
		return type;
	}

	public static int getHashcodeconstant() {
		return hashCodeConstant;
	}

	@Override
	public int hashCode() {
		return trainNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!(obj instanceof Train)) {
			return false;
		}

		Train castedObject = (Train) obj;

		if (trainNumber == castedObject.trainNumber) {
			return true;
		}

		return false;

	}

}
