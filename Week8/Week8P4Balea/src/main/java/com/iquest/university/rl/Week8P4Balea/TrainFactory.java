package com.iquest.university.rl.Week8P4Balea;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class TrainFactory {

	private Random random;
	private static final int MAX_RUNNING_DAYS_ALLOWED = 15;
	private static final int MAX_NR_DAYS = 365;
	private static final int MAX_NR_WAGONS = 30;

	public TrainFactory() {
		random = new Random();
	}

	public Map<Train, List<Integer>> getLuxuryTrainsSetUp(int numberOfTrains) {
		Map<Train, List<Integer>> returnedMap = new HashMap<Train, List<Integer>>();
		for (int index = 0; index < numberOfTrains; index++) {
			returnedMap.put(new Train(index + 1, (int) Math.random() * MAX_NR_WAGONS, "Luxury"),
					getCorrespondingRunningDays());
		}

		return returnedMap;
	}

	private List<Integer> getCorrespondingRunningDays() {
		List<Integer> returnedList = new ArrayList<Integer>();
		int randomMaxDaysPerTrain = random.nextInt(MAX_RUNNING_DAYS_ALLOWED) + 1;
		for (int index = 0; index < randomMaxDaysPerTrain; index++) {
			returnedList.add(random.nextInt(MAX_NR_DAYS) + 1);
		}

		return returnedList;
	}
}
