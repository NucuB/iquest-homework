package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.iquest.university.rl.Week8P4Balea.Train;
import com.iquest.university.rl.Week8P4Balea.TrainFactory;

public class App {
	public static void main(String[] args) {

		List<List<Integer>> retrievedRunningDaysLists = new ArrayList<List<Integer>>();
		int numberOfTrains = 10000;
		int numberOfCheckedTrains = 100;
		
		TrainFactory trainFactory = new TrainFactory();
		long nano_startTime = System.nanoTime();
		Map<Train, List<Integer>> trainMap = trainFactory.getLuxuryTrainsSetUp(numberOfTrains);

		for (int index = 0; index < numberOfCheckedTrains; index++) {
			retrievedRunningDaysLists.add(trainMap.get(new Train(index + 1, index, "Luxury")));
		}

		long nano_endTime = System.nanoTime();
		System.out.println("Time for retrieving all running days lists is : " + (nano_endTime - nano_startTime));

		for (int index = 0; index < retrievedRunningDaysLists.size(); index++) {
			System.out.println(retrievedRunningDaysLists.get(index));
		}
						// hashCode() returns constant
		// Time for retrieving all running days lists is : 1161538500
						// After changing the hashCode return;
		// Time for retrieving all running days lists is : 15511900
		// Time for retrieving all running days lists is : 9616200
	}
}
