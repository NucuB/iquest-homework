package com.iquest.university.rl.Week8P1Balea;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CountrySortTest {

	private List<Country> countries;
	private List<ExtendedCountry> extendedCountries;

	@Before
	public void setUp() {
		countries = new ArrayList<Country>();
		countries.add(new Country("Romania", "Bucharest"));
		countries.add(new Country("Turkey", "Ankara"));
		countries.add(new Country("Greece", "Atena"));

		extendedCountries = new ArrayList<ExtendedCountry>();
		extendedCountries.add(new ExtendedCountry("Romania", "Bucharest"));
		extendedCountries.add(new ExtendedCountry("Turkey", "Ankara"));
		extendedCountries.add(new ExtendedCountry("Greece", "Atena"));

	}

	@Test
	public void testSortingByName() {

		Collections.sort(countries);

		List<Country> expectedCountriesList = new ArrayList<Country>();
		expectedCountriesList.add(new Country("Greece", "Atena"));
		expectedCountriesList.add(new Country("Romania", "Bucharest"));
		expectedCountriesList.add(new Country("Turkey", "Ankara"));

		// WHEN
		String expectedResult = expectedCountriesList.toString();
		String actualResult = countries.toString();

		// THEN
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void testSortingByCapital() {
		Collections.sort(extendedCountries);

		List<ExtendedCountry> expectedCountriesList = new ArrayList<ExtendedCountry>();
		expectedCountriesList.add(new ExtendedCountry("Turkey", "Ankara"));
		expectedCountriesList.add(new ExtendedCountry("Greece", "Atena"));
		expectedCountriesList.add(new ExtendedCountry("Romania", "Bucharest"));

		// WHEN
		String actualResult = extendedCountries.toString();
		String expectedResult = expectedCountriesList.toString();

		// THEN
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void testBinarySearchForCapitalParis() {
		extendedCountries.add(new ExtendedCountry("South Africa", "Pretoria"));
		extendedCountries.add(new ExtendedCountry("France", "Paris"));
		// WHEN
		String expectedResult = extendedCountries.get(4).toString();

		ExtendedCountry searchedCountry = new ExtendedCountry("France", "Paris");

		Collections.sort(extendedCountries);
		int index = Collections.binarySearch(extendedCountries, searchedCountry);

		// THEN
		String actualResult = extendedCountries.get(index).toString();
		assertEquals(expectedResult, actualResult);

	}
}
