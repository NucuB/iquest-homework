package com.iquest.university.rl.Week8P1Balea;

public class Country implements Comparable<Country> {

	private String name;
	private String capital;

	public Country(String name, String capital) {
		this.name = name;
		this.capital = capital;
	}

	public String getName() {
		return name;
	}

	public String getCapital() {
		return capital;
	}

	public int compareTo(Country country) {

		if (!this.name.equalsIgnoreCase(country.getName())) {
			return this.name.compareTo(country.getName());
		}
		return 0;

	}

	@Override
	public String toString() {
		return "Country with name : " + name + " has capital : " + capital;
	}

}
