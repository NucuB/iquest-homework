package com.iquest.university.rl.Week8P1Balea;

public class ExtendedCountry extends Country {

	public ExtendedCountry(String name, String capital) {
		super(name, capital);
	}

	@Override
	public int compareTo(Country country) {

		if (!getCapital().equalsIgnoreCase(country.getCapital())) {
			return getCapital().compareToIgnoreCase((country.getCapital()));
		}

		return 0;
	}

}
