package com.iquest.university.rl.Week8P2Balea;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class DeckOfCards {

	private PlayingCard[] deck;
	private int size;
	private Random random;

	public DeckOfCards(int size) {
		this.size = size;
		deck = new PlayingCard[size];
		random = new Random();
	}

	public void fillDeckWithCards() {

		for (int index = 0; index < size; index++) {
			Suit suit = Suit.values()[random.nextInt(Suit.values().length)];
			deck[index] = new PlayingCard(index, suit);
		}
	}

	public void shuffleDeck() {
		List<PlayingCard> temporarlyDeck = Arrays.asList(deck);
		Collections.shuffle(temporarlyDeck);
		for (int index = 0; index < size; index++) {
			deck[index] = temporarlyDeck.get(index);
		}
		System.out.println("Deck shuffled");
	}

	public String testFunction() {
		String stringToBeBuild = "";
		for (int index = 0; index < size; index++) {
			stringToBeBuild += deck[index].toString();
		}
		return stringToBeBuild;
	}
}
