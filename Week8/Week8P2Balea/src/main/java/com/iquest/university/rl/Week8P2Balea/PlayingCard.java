package com.iquest.university.rl.Week8P2Balea;

public class PlayingCard {

	private int number;
	private Suit suit;
	
	public PlayingCard(int number, Suit suit) {
		this.number = number;
		this.suit = suit;
	}

	@Override
	public String toString() {
		return "Card with number : " + number + " of color : " + suit;
	}
	
	
	
}
