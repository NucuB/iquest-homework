package main;

import com.iquest.university.rl.Week8P2Balea.DeckOfCards;

public class App 
{
    public static void main( String[] args )
    {
        DeckOfCards deck = new DeckOfCards(10);
        deck.fillDeckWithCards();
        deck.shuffleDeck();
       
    }
}
