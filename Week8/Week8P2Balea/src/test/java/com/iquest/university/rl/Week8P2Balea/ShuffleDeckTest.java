package com.iquest.university.rl.Week8P2Balea;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.not;
import org.junit.Before;
import org.junit.Test;


public class ShuffleDeckTest {

	private DeckOfCards deck;
	
	
	@Before
	public void setUp() {
		deck = new DeckOfCards(10);
		deck.fillDeckWithCards();
	}
	
	
	@Test
	public void testDeckIsShuffled() {
		//WHEN
		String actualResult = deck.testFunction();
		
		deck.shuffleDeck();
		//THEN
		String expectedResult = deck.testFunction();
		assertThat(actualResult, is(not(expectedResult)));
		
		
	}
	
}
