package com.iquest.university.rl.Week8P3Balea;

import java.util.HashSet;
import java.util.Set;

public class TrainFactory {

	public Set<Train> getTrainSetUp(int numberOfTrains) {
		Set<Train> returnedSet = new HashSet<Train>();
		for (int index = 0; index < numberOfTrains; index++) {
			returnedSet.add(new Train(index + 1, index + 1, "Luxury"));
		}

		return returnedSet;
	}
}
