package com.iquest.university.rl.Week8P3Balea;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class TrainTest {

	private Set<Train> testedSet;
	private TrainFactory trainFactory;

	@Before
	public void setUp() {
		trainFactory = new TrainFactory();
		testedSet = trainFactory.getTrainSetUp(10);

	}

	@Test
	public void testIfSetHasDuplicates() {
		// WHEN
		int actualResult = testedSet.size();

		testedSet.add(new Train(1, 20, "Luxury"));
		testedSet.add(new Train(6, 30, "Economic"));
		testedSet.add(new Train(5, 10, "Luxury"));

		// THEN
		int expectedResult = testedSet.size();
		assertThat(actualResult, is((expectedResult)));

	}
}
