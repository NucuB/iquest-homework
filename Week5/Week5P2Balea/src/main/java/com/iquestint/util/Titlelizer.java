package com.iquestint.util;

public class Titlelizer implements ITitlelizer {

	//private String[] wordsToIgnore = { "a", "to", "is", "of", "the", "in" };
	private String[] wordsToIgnore;
	private StringBuilder stringBuilder;


	public Titlelizer(String[] wordsToIgnore) {
		stringBuilder = new StringBuilder();
		this.wordsToIgnore = wordsToIgnore;

	}
	
	public Titlelizer() {
		stringBuilder = new StringBuilder();

	}
	

	public String[] getWordsToIgnore() {
		return wordsToIgnore;
	}

	public String titlelize(String toTitlelize) {

		if ((toTitlelize == null) || (toTitlelize == " ")) {
			throw new IllegalArgumentException();
		}
		if (toTitlelize == "") {
			return "";
		}

		String[] stringAfterSplit = toTitlelize.split(" ");

		for (String string : stringAfterSplit) {
			Boolean hasToBeIgnored = false;

			for (String wordToIgnore : wordsToIgnore) {
				if (string.equals(wordToIgnore)) {
					hasToBeIgnored = true;
				}

			}
			if (!hasToBeIgnored) {
				if (string != stringAfterSplit[stringAfterSplit.length - 1]) {
					String stringAfterUpperCase = string.substring(0, 1).toUpperCase() + string.substring(1);
					stringBuilder.append(stringAfterUpperCase);
					stringBuilder.append(" ");
				} else {
					String stringAfterUpperCase = string.substring(0, 1).toUpperCase() + string.substring(1);
					stringBuilder.append(stringAfterUpperCase);
				}

			} else {
				stringBuilder.append(string);
				stringBuilder.append(" ");
			}

		}

		return stringBuilder.toString();
	}

}
