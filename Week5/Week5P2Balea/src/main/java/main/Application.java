package main;

import com.iquestint.util.Titlelizer;

public class Application {

	public static void main(String[] args) {
		String[] wordsToIgnore = { "a", "to", "is", "of", "the", "in" };

		Titlelizer titlelizer = new Titlelizer(wordsToIgnore);
		
		String titlelizedString = titlelizer.titlelize("this String is Titlelized");
		System.out.println(titlelizedString);
	}

}
