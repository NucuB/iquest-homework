package main;

import com.iQuestUniversity.rl.Week5P3Balea.RandomSentenceCreator;
import com.iQuestUniversity.rl.Week5P3Balea.Story;

public class App {

	public static void main(String[] args) {
		String[] article = { "the", "a", "one", "some", "any" };
		String[] noun = { "boy", "girl", "dog", "town", "car" };
		String[] verb = { "drove", "jumped", "ran", "walked", "skipped" };
		String[] preposition = { "to", "from", "over", "under", "on" };

		int numberOfGeneratedSentences = 20;
		RandomSentenceCreator randomSentenceCreator = new RandomSentenceCreator(article, noun, verb, preposition);
		Story randomStory = new Story(randomSentenceCreator);
		randomStory.createStory(numberOfGeneratedSentences);
	}
}
