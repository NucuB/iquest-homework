package com.iQuestUniversity.rl.Week5P3Balea;

import java.util.Random;

public class RandomSentenceCreator {

	
	private final String[] article;
	private final String[] noun;
	private final String[] verb;
	private final String[] preposition;
	
	private Random random;

	public RandomSentenceCreator(String[] article, String[] noun, String[] verb, String[] prepositions) {
		random = new Random();
		this.article = article;
		this.noun = noun;
		this.verb = verb;
		this.preposition = prepositions;
	}

	public String createSentence() {
		StringBuilder sentenceBuilder = new StringBuilder();
		String stringForCapitalizeFirstLetter = article[random.nextInt(5)];
		sentenceBuilder.append(stringForCapitalizeFirstLetter.substring(0, 1).toUpperCase()
				+ stringForCapitalizeFirstLetter.substring(1));
		sentenceBuilder.append(" ");
		sentenceBuilder.append(noun[random.nextInt(5)]);
		sentenceBuilder.append(" ");
		sentenceBuilder.append(verb[random.nextInt(5)]);
		sentenceBuilder.append(" ");
		sentenceBuilder.append(preposition[random.nextInt(5)]);
		sentenceBuilder.append(" ");
		sentenceBuilder.append(article[random.nextInt(5)]);
		sentenceBuilder.append(" ");
		sentenceBuilder.append(noun[random.nextInt(5)]);
		sentenceBuilder.append(".");
		return sentenceBuilder.toString();

	}
}
