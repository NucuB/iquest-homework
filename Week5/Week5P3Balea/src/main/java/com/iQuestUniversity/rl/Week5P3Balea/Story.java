package com.iQuestUniversity.rl.Week5P3Balea;

public class Story {

	private RandomSentenceCreator sentenceCreator;
	
	
	public Story(RandomSentenceCreator sentenceCreator) {
		this.sentenceCreator = sentenceCreator;
	}
	
	public void createStory(int numberOfSentencesToCreate) {
		for (int number = 0; number < numberOfSentencesToCreate; number++) {
			System.out.println(sentenceCreator.createSentence());
		}
	}
}
