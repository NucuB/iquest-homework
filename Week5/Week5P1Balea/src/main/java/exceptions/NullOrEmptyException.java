package exceptions;

public class NullOrEmptyException extends CustomException {

	public NullOrEmptyException(String message) {
		super(message);
	}

}
