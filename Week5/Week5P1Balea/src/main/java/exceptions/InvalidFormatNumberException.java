package exceptions;

public class InvalidFormatNumberException extends CustomException {

	public InvalidFormatNumberException(String message) {
		super(message);
	}

}
