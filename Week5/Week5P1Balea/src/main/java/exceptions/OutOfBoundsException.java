package exceptions;

public class OutOfBoundsException extends CustomException {

	public OutOfBoundsException(String message) {
		super(message);
	}

}
