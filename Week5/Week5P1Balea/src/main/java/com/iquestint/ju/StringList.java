package com.iquestint.ju;

import java.util.*;

import exceptions.InvalidFormatNumberException;
import exceptions.NullOrEmptyException;
import exceptions.OutOfBoundsException;

public class StringList implements List<String> {

	private int[] integerArray;
	private int sizeOfList;
	private int indexer;
	private int currentSize;
	private ArrayList<String> record;
	

	public StringList(int sizeOfList) {
		this.sizeOfList = sizeOfList;
		integerArray = new int[sizeOfList];
		record = new ArrayList<String>();
		indexer = 0;
		currentSize = 0;
	}

	
	public void add(String element) {
		int numberRepresentation;
		if (currentSize == integerArray.length) {
			System.out.println("Array is full, the capacity will be doubled");
			int newCapacity = currentSize * 2;
			integerArray =  Arrays.copyOf(integerArray, newCapacity);
			record.add("Capacity of array has been extended!");
			numberRepresentation = Integer.parseInt(element);
			integerArray[indexer] = numberRepresentation;
			record.add("Inserted element " + element + " at position : " + (indexer) + "-->list.add()");
			indexer++;
			currentSize++;

		} else if (element == null) {
			throw new NullOrEmptyException("Null");
		} else
			try {
				numberRepresentation = Integer.parseInt(element);
				integerArray[indexer] = numberRepresentation;
				record.add("Inserted element " + element + " at position : " + (indexer) + "-->list.add()");
				indexer++;
				currentSize++;
			} catch (NumberFormatException e) {
				throw new InvalidFormatNumberException("Invalid number.");
			}

	}

	public String get(int position) {

		if (position >= currentSize) {
			throw new OutOfBoundsException("Index out of bounds.");
		}

		return String.valueOf(integerArray[position]);

	}

	public boolean contains(String element) {
		int numberRepresentation = Integer.parseInt(element);
		for (int index = 0; index < integerArray.length; index++) {
			if (integerArray[index] == numberRepresentation) {
				return true;
			}
		}
		return false;
	}

	public boolean containsAll(List<String> foreignList) {
		boolean containsAll = true;

		for (int iterator = 0; iterator < foreignList.size(); iterator++) {
			String elementFromForeignList = foreignList.get(iterator);
			if (!contains(elementFromForeignList)) {
				containsAll = false;
			}

		}

		return containsAll;
	}

	public int size() {
		return currentSize;

	}

	public void showInformationAboutOperations() {
		for (String string : record) {
			System.out.println(string);
		}
	}
	
	public ArrayList<String> getRecord() {
		return record;
	}

	public int getSizeOfList() {
		return sizeOfList;
	}



}
