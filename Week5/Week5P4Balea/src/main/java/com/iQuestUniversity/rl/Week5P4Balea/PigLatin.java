package com.iQuestUniversity.rl.Week5P4Balea;

import java.util.StringTokenizer;

public class PigLatin {

	private StringBuilder stringBuilder;
	private StringTokenizer stringTokenizer;

	public void printLatinWord(String phrase) {
		System.out.println(getLatinWord(phrase));
	}

	public String getLatinWord(String phrase) {
		if (phrase == "") {
			return "";
		}
		if (phrase == null) {
			throw new IllegalArgumentException();
		}
		initData(phrase);
		int numberOfTokens = stringTokenizer.countTokens();
		int counter = 0;
		while (stringTokenizer.hasMoreTokens()) {
			String tokenFromTokenizer = stringTokenizer.nextToken();
			counter++;
			stringBuilder.append(tokenFromTokenizer.substring(1) + tokenFromTokenizer.charAt(0) + "ay");
			if (counter != numberOfTokens) {
				stringBuilder.append(" ");

			}
		}

		return stringBuilder.toString();
	}

	private void initData(String phraseForStringTokenizer) {
		stringTokenizer = new StringTokenizer(phraseForStringTokenizer);
		stringBuilder = new StringBuilder();
	}

	public StringTokenizer getStringTokenizer() {
		return stringTokenizer;
	}

}
