package main;

import java.util.Scanner;

import com.iQuestUniversity.rl.Week5P4Balea.PigLatin;

public class App {
	
	public static void main(String[] args) {
		
		boolean keepRunning = true;
		Scanner scanner = new Scanner(System.in);
		PigLatin pigLatinTraducer = new PigLatin();
		
		while (keepRunning) {
			System.out.print("Enter your phrase : ");
			String sentenceToBeTokenized = scanner.nextLine();
			pigLatinTraducer.printLatinWord(sentenceToBeTokenized);
			System.out.println("Would you like to enter one more sentence?   Y/N");
			String continueProgram = scanner.nextLine();
			if (continueProgram.equalsIgnoreCase("n")) {
				keepRunning = false;
				scanner.close();
				System.out.println("The pigLatinTraducer has stopped");
			}

		}
	}
		
}
