package com.iQuestUniversity.rl.Week5P4Balea;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class PigLatinTest {

	private PigLatin pigLatin;

	@Before
	public void setUp() {
		pigLatin = new PigLatin();
	}

	@Test
	public void testPhrase() {
		// WHEN
		String phrase = "Just go";
		String pigLatinString = pigLatin.getLatinWord(phrase);
		// THEN
		assertThat(pigLatinString, is("ustJay ogay"));
	}

	@Test
	public void testEmptyPhraseShouldReturnEmptyString() {
		// WHEN
		String actualResult = pigLatin.getLatinWord("");

		// THEN
		assertThat(actualResult, is(""));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNullPhraseShouldThrowIllegalArgumentException() throws Exception {
		pigLatin.getLatinWord(null);
	}

}
