package com.iquest.university.rl.Week10P2Balea;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;



public class ThreadRace {
	
	private int circuitLaps;
	private ThreadRaceContext raceContext;
	private ExecutorService exec;
	private int numberOfCompetitors;
	private Lock lock = new ReentrantLock();
	

	public ThreadRace(int numberOfCompetitors, int circuitLaps, ThreadRaceContext raceContext) {
		this.circuitLaps = circuitLaps;
		this.raceContext = raceContext;
		this.numberOfCompetitors = numberOfCompetitors;
		exec = Executors.newFixedThreadPool(numberOfCompetitors);

	}

	public void generateCompetitors() {
		for (int number = 0; number < numberOfCompetitors; number++) {
			exec.execute(new ThreadRaceCompetitor(number, this));
		}
	}
	
	public int getLaps() {
		return circuitLaps;
	}

	public ThreadRaceContext getRaceContext() {
		lock.lock();
		try {
			return raceContext;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
		return null;
	}

	public ExecutorService getExec() {
		return exec;
	}
	
	
	
	
	
}
