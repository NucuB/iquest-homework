package com.iquest.university.rl.Week10P2Balea;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadRaceCompetitor implements Runnable {

	private int competitorId;
	private int currentLap;
	private ThreadRace race;
	private Lock lock = new ReentrantLock();

	public ThreadRaceCompetitor(int id, ThreadRace race) {
		currentLap = 0;
		this.race = race;
		this.competitorId = id;
	}

	public void run() {

		while (currentLap <= race.getLaps()) {
			currentLap++;
		}

		informContext();

	}

	private void informContext() {
		lock.lock();

		try {
			race.getRaceContext().addRank("Thread competitor : " + competitorId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}

	}

}
