package com.iquest.university.rl.Week10P2Balea;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadRaceContext {

	private List<String> scoreCard;
	private Lock lock = new ReentrantLock();
	

	public ThreadRaceContext() {
		scoreCard = new ArrayList<String>();
	}

	public List<String> getScoreCard() {
		return scoreCard;
	}
	
	public void addRank(String string) {
		lock.lock();
		try {
			scoreCard.add(string);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
	}
	
	
	
}
