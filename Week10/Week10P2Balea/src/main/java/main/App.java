package main;

import com.iquest.university.rl.Week10P2Balea.ThreadRace;
import com.iquest.university.rl.Week10P2Balea.ThreadRaceContext;

public class App 
{
    public static void main( String[] args )
    {
    	ThreadRaceContext context = new ThreadRaceContext();
    	ThreadRace race = new ThreadRace(10, 5, context);
    	race.generateCompetitors();
    	race.getExec().shutdown();
    	System.out.println(context.getScoreCard().toString());
    }
}
