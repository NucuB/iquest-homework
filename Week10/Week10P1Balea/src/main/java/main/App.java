package main;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.iquest.university.rl.Week10P1Balea.Consumer;
import com.iquest.university.rl.Week10P1Balea.Producer;
import com.iquest.university.rl.Week10P1Balea.Server;

public class App {
	public static void main(String[] args) {
		Server server = new Server(10);
		ExecutorService exec = Executors.newCachedThreadPool();
		int numberOfProducers = 3;
		exec.execute(new Consumer(server));

		for (int number = 0; number < numberOfProducers; number++) {
			Future<?> producerStatus = exec.submit(new Producer(server));
			try {
				producerStatus.get();

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		exec.shutdown();

	}
}
