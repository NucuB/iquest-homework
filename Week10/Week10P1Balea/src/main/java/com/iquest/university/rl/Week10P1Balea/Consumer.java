package com.iquest.university.rl.Week10P1Balea;

public class Consumer implements Runnable {

	private Server server;

	public Consumer(Server server) {
		this.server = server;
	}

	public void run() {

		for (int counter = 0; counter < server.getSize(); counter++) {
			server.consumeMessage();
		}

	}

}
