package com.iquest.university.rl.Week10P1Balea;

import java.util.LinkedList;
import java.util.List;

public class Server {

	private List<String> messages;
	private int size;
	private int counterProducer = 0;
	private int counterConsumer = 0;

	public Server(int size) {
		messages = new LinkedList<String>();
		this.size = size;
	}

	public synchronized void produceMessage() {

		while (messages.size() == size) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String message = "New message added in list";
		messages.add(message);
		System.out.println("Counter producer : " + counterProducer++ + " produced : " + message);
		notify();
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized void consumeMessage() {
		while (true) {
			
			while (messages.size() == 0) {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			String value = ((LinkedList<String>) messages).removeFirst();
			System.out.println("Value removed : [ " + value + " ]");
			System.out.println("Counter consumer : " + counterConsumer++);

			notify();
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public synchronized int getSize() {
		return size;
	}

	public synchronized List<String> getMessages() {
		return messages;
	}

}
