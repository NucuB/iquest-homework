package com.iquest.university.rl.Week10P3Balea;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadCompetitor implements Runnable {

	private int id;
	private String teamName;
	private int currentLap;
	private ThreadRelayRaceTeam team;
	//private static CyclicBarrier barrier;
	private Lock lock = new ReentrantLock();
	

	public ThreadCompetitor(int id, String teamName, ThreadRelayRaceTeam team ) {
		this.id = id;
		this.teamName = teamName;
		currentLap = 0;
		this.team = team;
	}
	
	public void run() {
		while (!Thread.interrupted()) {
			synchronized (this) {
				while (currentLap <= team.getRace().getCircuitLaps()) {
					currentLap++;
				}
				
			}
			/*try {
				barrier.await();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
		informContext();
		
	}
	
	private void informContext() {
		lock.lock();

		try {
			team.getRace().getContext().addRank("Thread competitor : " + id + " from team : " + team.getTeamName());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}

	}

}
