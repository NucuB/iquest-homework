package com.iquest.university.rl.Week10P3Balea;

public class ThreadRelayRace {

	private final String[] enrolledTeams;
	private int numberOfTeams;
	private int circuitLaps;
	private ThreadRaceContext context;
	
	public ThreadRelayRace(int numberOfTeams, String[] enrolledTeams, int circuitLaps, ThreadRaceContext context) {
		this.numberOfTeams = numberOfTeams;
		this.enrolledTeams = enrolledTeams;
		this.circuitLaps = circuitLaps;
		this.context = context;
	}
	

	public void startRace() {
		for (int number = 0; number < numberOfTeams; number++) {
			ThreadRelayRaceTeam raceTeam = new ThreadRelayRaceTeam(4,enrolledTeams[number], this);
			Thread[] competitorsOfRaceTeam = raceTeam.getThreadTeam();
			for (int index = 0; index < competitorsOfRaceTeam.length; index++) {
				competitorsOfRaceTeam[index].start();
			}
			for (int index = 0; index < competitorsOfRaceTeam.length; index++) {
				try {
					competitorsOfRaceTeam[index].join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

	public String[] getEnrolledTeams() {
		return enrolledTeams;
	}

	public int getNumberOfTeams() {
		return numberOfTeams;
	}
	
	public synchronized int getCircuitLaps() {
		return circuitLaps;
	}


	public ThreadRaceContext getContext() {
		return context;
	}
	
}
