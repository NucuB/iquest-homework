package com.iquest.university.rl.Week10P3Balea;

public class ThreadRelayRaceTeam {

	private int numberOfCompetitors;
	private String teamName;
	private Thread[] threadTeam;
	private ThreadRelayRace race;
	
	public ThreadRelayRaceTeam(int numberOfCompetitors, String teamName, ThreadRelayRace race) {
		this.numberOfCompetitors = numberOfCompetitors;
		this.teamName = teamName;
		this.race = race;
		this.threadTeam = new Thread[this.numberOfCompetitors];
		prepareTeamCompetitors();
	}
	
	private void prepareTeamCompetitors() {
		for (int number = 0; number < numberOfCompetitors; number++) {
			threadTeam[number] = new Thread( new ThreadCompetitor(number, teamName, this));
		}
	}

	public Thread[] getThreadTeam() {
		return threadTeam;
	}

	public int getNumberOfCompetitors() {
		return numberOfCompetitors;
	}

	public String getTeamName() {
		return teamName;
	}

	public synchronized ThreadRelayRace getRace() {
		return race;
	}
	
	
	
	
	
	
}
