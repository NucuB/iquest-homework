package main;

import com.iquest.university.rl.Week10P3Balea.ThreadRaceContext;
import com.iquest.university.rl.Week10P3Balea.ThreadRelayRace;

public class App 
{
    public static void main( String[] args )
    {
    	String[] enrolledTeams = {"A","B","C","D","E","F","G","H","I","J"};
    	int circuitLaps = 5;
    	
    	ThreadRaceContext context = new ThreadRaceContext();
    	ThreadRelayRace race = new ThreadRelayRace(10, enrolledTeams, circuitLaps, context);
    	race.startRace();
    
    	System.out.println(context.getScoreCard().toString());
    }
}
