package com.iquest.university.Week14P3Balea;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Before;
import org.junit.Test;

public class FooTest {

	private Foo foo;
	
	@Before
	public void setUp() {
		foo = new Foo();
	}
	@Test(expected = Exception.class)
	public void testWithLockMethodShouldReturnString() {
		Lock lock = new ReentrantLock();
		foo.withLock(lock, () -> {
			int number = 10;
			String text = "...";
			int conv = Integer.parseInt(text);
			int result = number / conv;
			System.out.println("Result : " + result);
		});
	}
		
}
