package main;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.iquest.university.Week14P3Balea.Foo;
import com.iquest.university.Week14P3Balea.IFoo;

public class App {
	public static void main(String[] args) {
		Lock lock = new ReentrantLock();
		Foo foo = new Foo();
		foo.withLock(lock, () -> {
			int number1 = 10;
			String text = "...";
			int conv = Integer.parseInt(text);
			int number2 = 5;
			int result = number1 / number2;
			System.out.println("Result : " + result);
		});

	}
}
