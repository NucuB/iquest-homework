package com.iquest.university.Week14P3Balea;

import java.util.concurrent.locks.Lock;

public class Foo {

	public void withLock(Lock lock, IFoo function) {
		lock.lock();
		try {
			function.run();
		} catch (Exception e) {
			throw new IllegalArgumentException();
		} finally {
			lock.unlock();
		}
		
	}
}
