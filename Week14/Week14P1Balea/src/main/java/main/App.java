package main;

import java.io.File;

import com.iquest.university.Week14P1Balea.FileUtility;

public class App 
{
    public static void main( String[] args )
    {
    	String givenDirectory = "testFolder";
        FileUtility utility = new FileUtility(givenDirectory);
        File[] paths = utility.getSubdirectoriesUsingMethodExpression();
        for (File path : paths) {
        	System.out.println(path);
        }
        
        paths = utility.getSubdirectoriesUsingLambda();
        for (File path : paths) {
        	System.out.println(path);
        }
    }
}
