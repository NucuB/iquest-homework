package com.iquest.university.Week14P1Balea;

import java.io.File;

public class FileUtility {
	
	private String pathToDirectory;
	
	public FileUtility(String directoryName) {
		pathToDirectory = this.getClass().getClassLoader().getResource(directoryName).getPath();
	}

	public File[] getSubdirectoriesUsingLambda() {
		File givenDirectoryPath = new File(pathToDirectory);
		File[] paths = givenDirectoryPath.listFiles((path) -> path.isDirectory());
		
		return paths;
		
	}
	
	public File[] getSubdirectoriesUsingMethodExpression() {
		File givenDirectoryPath = new File(pathToDirectory);
		File[] paths = givenDirectoryPath.listFiles(File::isDirectory);
		return paths;
		
	}
}
