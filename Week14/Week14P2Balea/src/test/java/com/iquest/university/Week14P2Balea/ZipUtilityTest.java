package com.iquest.university.Week14P2Balea;

import static org.junit.Assert.assertEquals;

import java.util.stream.Stream;

import org.junit.Before;
import org.junit.jupiter.api.Test;

public class ZipUtilityTest {
	private Integer[] firstArray = {1,2,3,4,5};
	private Integer[] secondArray = {10,30,20,45};
	@Before
	public void setUp() {
		
	}
	
	@Test
	public void testMethod() {
		
		Stream<Integer> firstStream = Stream.of(firstArray);
		Stream<Integer> secondStream = Stream.of(secondArray);
		
		Stream<Integer> resultStream = ZipUtility.zip(firstStream, secondStream);
		long actualSize =  resultStream.count();
		long expectedSize = firstArray.length + secondArray.length;
		
		assertEquals(expectedSize, actualSize);
		
	}
}
