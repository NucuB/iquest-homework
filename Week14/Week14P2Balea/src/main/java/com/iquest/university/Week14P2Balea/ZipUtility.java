package com.iquest.university.Week14P2Balea;

import java.util.Iterator;
import java.util.stream.Stream;

public class ZipUtility {
	
	
	public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
		Stream.Builder<T> builder = Stream.builder(); 
		Iterator<T> it1 = first.iterator();
		Iterator<T> it2 = second.iterator();
		while (it1.hasNext()) {
			builder.add(it1.next());
			if (it2.hasNext()) {
				builder.add(it2.next());
			} else {
				break;
			}
		}
		Stream<T> returnedStream = builder.build(); 
		return returnedStream;
		
	}
	
}
