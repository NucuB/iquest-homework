package com.iquest.university.Week14P4Balea;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Utility {

	public List<Person> getAllPersonsOverAge(List<Person> persons, int limit) {
		List<Person> personsFiltered = 
				persons
					.stream()
					.filter(person -> person.getAge() > limit)
					.collect(Collectors.toList());
		return personsFiltered;
	}

	public Person getOldestPerson(List<Person> persons) {
		Person oldestPerson =
				persons
					.stream()
					.max(Comparator.comparing(Person::getAge))
					.get();
		return oldestPerson;
	}
	
	public boolean checkAllPersonsAreYoungerThan(List<Person> persons, int limit) {
		boolean allAreYounger =
				persons
					.stream()
					.allMatch(person -> person.getAge() < limit);
		return allAreYounger;
	}

	public void printPersonsGroupedByAge(List<Person> persons) {
		Map<Integer, List<Person>> resultMap =
				persons
					.stream()
					.collect(Collectors.groupingBy(Person::getAge));
		System.out.println(resultMap);
	}

}
