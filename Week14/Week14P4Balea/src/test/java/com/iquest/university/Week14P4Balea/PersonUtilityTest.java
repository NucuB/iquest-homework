package com.iquest.university.Week14P4Balea;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PersonUtilityTest {

	private Utility personUtility;
	private List<Person> persons;

	@Before
	public void setUp() {
		persons = Arrays.asList(new Person("Jane", 18), new Person("Simone", 18), new Person("John", 17),
				new Person("Marcel", 15), new Person("Andrew", 21), new Person("Abby", 20));

		personUtility = new Utility();
	}

	@Test
	public void testGetPersonsOlderThan() {
		int ageLimit = 18;
		List<Person> persons = personUtility.getAllPersonsOverAge(this.persons, ageLimit);
		int actualResult = persons.size();
		int expectedResult = 2;
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void testGetOldestPerson() {
		Person expectedPerson = new Person("Andrew", 21);
		Person actualPerson = personUtility.getOldestPerson(persons);
		String expectedName = expectedPerson.getName();
		String actualName = actualPerson.getName();
		assertEquals(expectedName, actualName);
	}

	@Test
	public void testCheckAllPersonsAreYoungerThan() {
		int limit = 80;

		boolean actualResult = personUtility.checkAllPersonsAreYoungerThan(persons, limit);
		boolean expectedResult = true;
		assertEquals(expectedResult, actualResult);
	}

	@Test
	public void printPersonsGroupedByAge() {
		personUtility.printPersonsGroupedByAge(persons);
	}

}
