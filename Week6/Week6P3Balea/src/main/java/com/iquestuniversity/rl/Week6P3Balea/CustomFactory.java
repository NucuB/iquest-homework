package com.iquestuniversity.rl.Week6P3Balea;

import java.net.URISyntaxException;

public class CustomFactory {

	public Object getInitialOrReloadedClassObject()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, URISyntaxException {

		String urlForExternalProject = "file:"
				+ this.getClass().getClassLoader().getResource("MyClass.class").getPath();

		ClassLoader parentClassLoader = CustomClassLoader.class.getClassLoader();

		Class loadedClassByParent = parentClassLoader.loadClass("com.iquestuniversity.rl.Week6P3Balea.MyClass");

		MyClass objectMyClassByParent = (MyClass) loadedClassByParent.newInstance();

		Class loadedSubClassByParent = parentClassLoader
				.loadClass("com.iquestuniversity.rl.Week6P3Balea.MySubClass");

		MyClass objectMySubClassByParent = (MySubClass) loadedSubClassByParent.newInstance();

		CustomClassLoader customClassLoaderForExternal = new CustomClassLoader(parentClassLoader,
				urlForExternalProject);
		Class loadedClassFromExternal = customClassLoaderForExternal.loadClass("com.iquest.university.rl.MyClass");
		Object objectFromExternal = loadedClassFromExternal.newInstance();

		
		return objectMySubClassByParent;
	}
}
