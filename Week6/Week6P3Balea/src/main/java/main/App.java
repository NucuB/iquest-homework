package main;

import java.net.URISyntaxException;

import com.iquestuniversity.rl.Week6P3BaleaVersion2.CustomFactory;
import com.iquestuniversity.rl.Week6P3BaleaVersion2.MyClass;

public class App {
	public static void main(String[] args)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, URISyntaxException {
		
		MyClass objectMyClass = new MyClass();
		String wantedClass = "Subclass";
		String externalPackage = "com.iquest.university.rl";

		CustomFactory factory = new CustomFactory(objectMyClass, externalPackage);
		Class<?> classFromFactory = (Class<?>) factory.getInitialOrReloadedClass(wantedClass);
		
		MyClass test = (MyClass) classFromFactory.newInstance();
		System.out.println(test.getString());

	}
}
