package com.iquestuniversity.rl.Week6P3BaleaVersion2;

import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;



public class AppTest {

	private MyClass myClassObject;
	private CustomFactory customFactory;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() {
		myClassObject = new MyClass();
		customFactory = new CustomFactory(myClassObject);

	}

	@Test
	public void testReturnedObjectIsInstanceOfMyClass() {
		// WHEN

		MyClass objectMyClass = null;
		try {
			objectMyClass = (MyClass) customFactory.getInitialOrReloadedClass("").newInstance();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// THEN
		Assert.assertTrue(objectMyClass instanceof MyClass);

	}

	
	//expect ClassCastException didn't work
	@Test(expected = AssertionError.class)
	public void testThrowClassCastException() {
		exception.expect(AssertionError.class);
      
       try {
		MyClass objectMyClass = (MyClass) customFactory.getInitialOrReloadedClass("MyClass").newInstance();
	} catch (InstantiationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (URISyntaxException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	// Fail test
	/*
	 * @Test public void testReturnedObjectIsInstanceOfMySubClass() {
	 * 
	 * MySubclass objectMySubclass = new MySubclass(); try { objectMySubclass =
	 * (MySubclass) customFactory.getInitialOrReloadedClassObject(); } catch
	 * (ClassNotFoundException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (InstantiationException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } catch
	 * (IllegalAccessException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (URISyntaxException e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } // THEN Assert.assertTrue(objectMySubclass
	 * instanceof MySubclass); }
	 */
}
