package com.iquestuniversity.rl.Week6P3Balea;

import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CustomFactoryTest {

	private CustomFactory customFactory;

	@Before
	public void setUp() {
		customFactory = new CustomFactory();

	}

	@Test
	public void testReturnedObjectIsInstanceOfMyClass() {
		// WHEN

		MyClass objectMyClass = new MyClass();
		try {
			objectMyClass = (MyClass) customFactory.getInitialOrReloadedClassObject();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// THEN
		Assert.assertTrue(objectMyClass instanceof MyClass);

	}

	@Test
	public void testReturnedObjectIsInstanceOfMySubClass() {

		MySubClass objectMySubclass = new MySubClass();
		try {
			objectMySubclass = (MySubClass) customFactory.getInitialOrReloadedClassObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// THEN
		Assert.assertTrue(objectMySubclass instanceof MySubClass);
	}
}
