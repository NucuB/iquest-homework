package com.iQuestUniversity.rl.Week6P1Balea;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

//import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertThat;

public class DateImplTest {

	private Date customizedDate;
	private LocalDate localDate;

	@Before
	public void setUp() {
		localDate = LocalDate.now();
		customizedDate = new Date(localDate);
		
	}

	@Test
	public void testMonth() {
		// WHEN
		MonthsOfYear currentMonth = customizedDate.getMonth();

		// THEN
		assertThat(currentMonth.toString(), is(localDate.getMonth().toString()));

	}

	@Test
	public void testDay() {

		// WHEN
		DaysOfWeek currentDay = customizedDate.getDay();

		// THEN
		assertThat(currentDay.toString(), is(localDate.getDayOfWeek().toString()));
	}

}
