package main;

import java.time.LocalDate;

import com.iQuestUniversity.rl.Week6P1Balea.Date;

public class App 
{
    public static void main( String[] args )
    {
    	LocalDate givenDate = LocalDate.now();
    	
    	Date customizedDate = new Date(givenDate);
    
    	
    	System.out.println(customizedDate);
    }
}
