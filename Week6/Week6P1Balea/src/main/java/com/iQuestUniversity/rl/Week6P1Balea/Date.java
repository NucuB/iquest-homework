package com.iQuestUniversity.rl.Week6P1Balea;

import java.time.LocalDate;

public class Date {

	private DaysOfWeek day;
	private MonthsOfYear month;
	private LocalDate currentDate;

	public Date(LocalDate givenDate) {
		currentDate = givenDate;
		initializeDayOfTheWeek();
		initializeMonthOfTheYear();

	}

	private void initializeDayOfTheWeek() {

		switch (currentDate.getDayOfWeek()) {

		case MONDAY:
			day = DaysOfWeek.MONDAY;
			break;

		case TUESDAY:
			day = DaysOfWeek.TUESDAY;
			break;

		case WEDNESDAY:
			day = DaysOfWeek.WEDNESDAY;
			break;

		case THURSDAY:
			day = DaysOfWeek.THURSDAY;
			break;

		case FRIDAY:
			day = DaysOfWeek.FRIDAY;
			break;

		case SATURDAY:
			day = DaysOfWeek.SATURDAY;
			break;

		case SUNDAY:
			day = DaysOfWeek.SUNDAY;
			break;

		}
	}

	private void initializeMonthOfTheYear() {

		switch (currentDate.getMonth()) {

		case JANUARY:
			month = MonthsOfYear.JANUARY;
			break;

		case FEBRUARY:
			month = MonthsOfYear.FEBRUARY;
			break;

		case MARCH:
			month = MonthsOfYear.MARCH;
			break;

		case APRIL:
			month = MonthsOfYear.APRIL;
			break;

		case MAY:
			month = MonthsOfYear.MAY;
			break;

		case JUNE:
			month = MonthsOfYear.JUNE;
			break;

		case JULY:
			month = MonthsOfYear.JULY;
			break;

		case AUGUST:
			month = MonthsOfYear.AUGUST;
			break;

		case SEPTEMBER:
			month = MonthsOfYear.SEPTEMBER;
			break;

		case OCTOBER:
			month = MonthsOfYear.OCTOBER;
			break;

		case NOVEMBER:
			month = MonthsOfYear.NOVEMBER;
			break;

		case DECEMBER:
			month = MonthsOfYear.DECEMBER;
			break;
		}

	}

	public DaysOfWeek getDay() {
		return day;
	}

	public MonthsOfYear getMonth() {
		return month;
	}

	@Override
	public String toString() {

		return "Today is : " + day + " in month :  " + month + " of year : " + currentDate.getYear();
	}

}
