package com.iquestuniversity.rl.Week6P2Balea;

@Logged(enabled = true)
public class Singer implements IArtist {

	private String name;

	public Singer(String name) {
		this.name = name;
	}

	@Logged(enabled = true)
	public void perform(String message, String anotherMessage) {
		System.out.println(message);
		System.out.println(anotherMessage);
	}

	public void unannotatedMethod() {
		System.out.println("Unannotated method");

	}

	public String getName() {
		return name;
	}

}
