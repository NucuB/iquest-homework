package com.iquestuniversity.rl.Week6P2Balea;

import java.lang.reflect.Proxy;

import exceptions.InvalidInterfaceException;

public class ProxyFactory {



	public Object getProxyInstance(Object object, Class<?>[] ifaces) {
		for (Class<?> iface : ifaces) {
			if (iface.isAssignableFrom(object.getClass())) {
				return Proxy.newProxyInstance(object.getClass().getClassLoader(), ifaces, new DynamicProxyHandler(object));
			} else {
				throw new InvalidInterfaceException("The class does not implement any specified interface");
			}
		}
		
		return null;
		

	}
}
