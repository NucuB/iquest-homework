package com.iquestuniversity.rl.Week6P2Balea;

@Logged
public interface IArtist {

	@Logged
	public void perform(String unusedParameter, String secondUnusedParameter);
	
	public void unannotatedMethod();
}
