package exceptions;

public class InvalidInterfaceException extends RuntimeException {

	public InvalidInterfaceException(String message) {
		super(message);
	}
}
