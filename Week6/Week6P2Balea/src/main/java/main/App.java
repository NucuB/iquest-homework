package main;

import com.iquestuniversity.rl.Week6P2Balea.IArtist;
import com.iquestuniversity.rl.Week6P2Balea.ProxyFactory;
import com.iquestuniversity.rl.Week6P2Balea.Singer;

public class App {

	public static void main(String[] args) {

		IArtist singer = new Singer("John");
		ProxyFactory proxyFactory = new ProxyFactory();
		singer = (IArtist) proxyFactory.getProxyInstance(singer, new Class[] {IArtist.class});
		singer.perform("first song", "second song");
		singer.unannotatedMethod();
		

	}
}
