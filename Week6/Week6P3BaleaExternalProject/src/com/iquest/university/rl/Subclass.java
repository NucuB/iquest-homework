package com.iquest.university.rl;

public class Subclass extends MyClass  {

	public Subclass() {
		System.out.println("Constructor from Subclass");
	}
	public String getString() {
		return "SUBCLASS";
	}
}
