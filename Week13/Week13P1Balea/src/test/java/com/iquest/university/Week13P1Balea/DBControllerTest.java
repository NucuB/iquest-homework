package com.iquest.university.Week13P1Balea;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import model.Operation;
import model.User;

public class DBControllerTest {

	private DBController database;
	private DBConnection connection;
	private String configFile;
	@Before
	public void setUp() {
		configFile = "config.txt";
		connection = new DBConnection(configFile);
		database = new DBController(connection);
		User user1 = new User(1,"Ion Popescu","ipop", 2500);
		User user2 = new User(2,"Marius Popescu","mpop", 200000);
		User user3 = new User(3,"Jon Pop","jpop", 50000);
		try {
			database.insertUserIntoDatabase(user1);
			database.insertUserIntoDatabase(user2);
			database.insertUserIntoDatabase(user3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testRetrieveClientsShouldNotBeEmptyList() {
		List<User> users = database.retrieveClients();
		int unexpectedSize = 0;
		int actualSize = users.size();
		assertNotEquals(unexpectedSize, actualSize);
	}

	@Test
	public void testRetrieveBalanceOfSpecifiedClient() {
		String clientName = "Marius Popescu";
		int actualBalance = database.retrieveBalanceOfClient(clientName);
		int expectedBalance = 200000;
		assertEquals(expectedBalance, actualBalance);
	}

	@Test
	public void testRetrieveAllClientsWithBalanceBiggerThan() {
		int limit = 100000;
		List<User> users = database.retrieveClientsWithMoreMoneyThan(limit);
		int unexpectedSize = 0;
		int actualSize = users.size();
		assertNotEquals(unexpectedSize, actualSize);
	}

	@Test
	public void testPerformGivenOperationShouldUpdateDatabase() {
		Operation raiseBalanceOp = new Operation(500, "Salary", 1);
		String clientName = "Ion Popescu";
		int oldBalance = database.retrieveBalanceOfClient(clientName);
		try {
			database.updateDatabaseAccordinglyGivenOperation(raiseBalanceOp);
		} catch (SQLException e) {
			e.printStackTrace();
		};
		int currentBalance = database.retrieveBalanceOfClient(clientName);
		int expectedBalance = oldBalance + raiseBalanceOp.getAmount();
		assertEquals(expectedBalance, currentBalance);
		

	}

	@Test
	public void testGetDatabaseNameTablesAndColumns() {
		try {
			List<String> databaseColumns = database.getDatabaseNameTablesAndColumns();
			int actualSizeColumns = databaseColumns.size();
			int expectedSizeColumns = 4;
			assertEquals(expectedSizeColumns, actualSizeColumns);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
