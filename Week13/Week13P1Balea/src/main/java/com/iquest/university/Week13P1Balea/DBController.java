package com.iquest.university.Week13P1Balea;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exception.CannotExecuteTransaction;
import exception.CannotOperateWithConnectionException;
import exception.ConnectionNullException;
import model.Operation;
import model.User;

public class DBController {

	private DBConnection connection;

	public DBController(DBConnection connection) {
		this.connection = connection;
	}

	// Problem 1
	public List<User> retrieveClients() {
		throwExceptionIfConnectionIsNull();
		List<User> users = new ArrayList<User>();
		try {
			String querry = "SELECT * FROM user";
			PreparedStatement preparedStatement = connection.getConnection().prepareStatement(querry);
			ResultSet resultSet = preparedStatement.executeQuery();
			users = getUsersByprocessingResultSet(resultSet);
		} catch (Exception e) {
			throw new CannotOperateWithConnectionException("Failed to operate with database");
		}
		return users;
	}

	// Problem 1
	public int retrieveBalanceOfClient(String clientName) {
		throwExceptionIfConnectionIsNull();
		int balance = 0;
		try {
			String querry = "SELECT balance FROM user WHERE name = ?";
			PreparedStatement preparedStatement = connection.getConnection().prepareStatement(querry);
			preparedStatement.setString(1, clientName);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				balance = resultSet.getInt("balance");
			}
		} catch (Exception e) {
			throw new CannotOperateWithConnectionException("Failed to operate with database");
		}
		return balance;
	}

	// Problem 1
	public List<User> retrieveClientsWithMoreMoneyThan(int limit) {
		throwExceptionIfConnectionIsNull();
		List<User> users = new ArrayList<User>();
		try {
			String querry = "SELECT * FROM user WHERE balance > ?";
			PreparedStatement preparedStatement = connection.getConnection().prepareStatement(querry);
			preparedStatement.setInt(1, limit);
			ResultSet resultSet = preparedStatement.executeQuery();
			users = getUsersByprocessingResultSet(resultSet);
		} catch (Exception e) {
			throw new CannotOperateWithConnectionException("Failed to operate with database");
		}
		return users;
	}

	// Problem 2
	public void updateDatabaseAccordinglyGivenOperation(Operation operation) throws SQLException {
		throwExceptionIfConnectionIsNull();
		try {
			insertNewOperation(operation);
			int operationAmount = operation.getAmount();
			int userId = operation.getUserId();
			User userWithSpecifiedId = retrieveClientWithSpecifiedId(userId);
			int currentAmountOfUser = userWithSpecifiedId.getBalance();
			int newAmountOfUser = currentAmountOfUser + operationAmount;
			userWithSpecifiedId.setBalance(newAmountOfUser);
			updateUserBalance(userWithSpecifiedId);
		} catch (SQLException e) {
			throw new CannotExecuteTransaction("Transaction failed");
		}

	}

	// Problem 3
	public List<String> getDatabaseNameTablesAndColumns() throws SQLException {
		throwExceptionIfConnectionIsNull();
		String databaseName = getDatabaseName();
		List<String> tablesNames = getDatabaseTables();
		List<String> columnsNames = getDatabaseColumns();
		System.out.println("Database name : " + databaseName);
		System.out.println("Database tables : " + tablesNames.toString());
		System.out.println("Database columns : " + columnsNames.toString());
		return columnsNames;
	}

	public void insertUserIntoDatabase(User user) throws SQLException {
		throwExceptionIfConnectionIsNull();
		String querry = "INSERT INTO user VALUES(?,?,?,?)";
		try {
			PreparedStatement preparedStatement = connection.getConnection().prepareStatement(querry);
			preparedStatement.setInt(1, user.getId());
			preparedStatement.setString(2, user.getName());
			preparedStatement.setString(3, user.getUserName());
			preparedStatement.setInt(4, user.getBalance());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// throw new CannotOperateWithConnectionException("Failed to operate with
			// database");
		}
	}

	private void updateUserBalance(User user) throws SQLException {
		String querry = "UPDATE user SET balance = ?  WHERE id = " + user.getId();
		try {
			PreparedStatement ps = connection.getConnection().prepareStatement(querry);
			ps.setInt(1, user.getBalance());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new CannotOperateWithConnectionException("Failed to operate with database");
		}
	}

	private User retrieveClientWithSpecifiedId(int userId) throws SQLException {
		User user = null;

		String querry = "SELECT * FROM user WHERE id = ?";
		PreparedStatement preparedStatement = connection.getConnection().prepareStatement(querry);
		preparedStatement.setInt(1, userId);
		ResultSet resultSet = preparedStatement.executeQuery();
		user = getUserByprocessingResultSet(resultSet);

		return user;
	}

	private String getDatabaseName() throws SQLException {
		String name = "";
		name = connection.getConnection().getCatalog();
		return name;
	}

	private List<String> getDatabaseTables() throws SQLException {
		String tablesTypes[] = { "TABLE" };
		List<String> tablesNames = new ArrayList<String>();
		DatabaseMetaData metaData = (DatabaseMetaData) connection.getConnection().getMetaData();

		ResultSet resultSet = metaData.getTables(null, null, null, tablesTypes);
		while (resultSet.next()) {
			tablesNames.add(resultSet.getString("TABLE_NAME"));
		}
		return tablesNames;
	}

	private List<String> getDatabaseColumns() throws SQLException {
		List<String> columnsNames = new ArrayList<String>();
		String querry = "SELECT * FROM user";

		PreparedStatement preparedStatement = connection.getConnection().prepareStatement(querry);
		ResultSet resultSet = preparedStatement.executeQuery();
		ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
		int columnCount = resultSetMetaData.getColumnCount();
		for (int index = 1; index <= columnCount; index++) {
			columnsNames.add("Column : " + index + " = " + resultSetMetaData.getColumnName(index));
		}
		return columnsNames;
	}

	private void insertNewOperation(Operation operation) throws SQLException {
		throwExceptionIfConnectionIsNull();

		String querry = "INSERT INTO operation VALUES(?,?,?,?,?)";

		try {
			PreparedStatement preparedStatement = connection.getConnection().prepareStatement(querry);
			// There are 5 columns in operation table,so I must set all of them,not only
			// amount,description and user_id
			preparedStatement.setURL(1, null);
			preparedStatement.setInt(2, operation.getAmount());
			preparedStatement.setString(3, operation.getDescription());
			preparedStatement.setURL(4, null);
			preparedStatement.setInt(5, operation.getUserId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new CannotOperateWithConnectionException("Failed to operate with database");
		}
	}

	private List<User> getUsersByprocessingResultSet(ResultSet resultSet) throws SQLException {
		List<User> users = new ArrayList<User>();
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String name = resultSet.getString("name");
			String userName = resultSet.getString("username");
			int balance = resultSet.getInt("balance");
			User user = new User(id, name, userName, balance);
			users.add(user);
		}
		return users;
	}

	private User getUserByprocessingResultSet(ResultSet resultSet) throws SQLException {
		User user = null;
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String name = resultSet.getString("name");
			String userName = resultSet.getString("username");
			int balance = resultSet.getInt("balance");
			user = new User(id, name, userName, balance);
		}
		return user;
	}

	private void throwExceptionIfConnectionIsNull() {
		if (connection.getConnection() == null) {
			throw new ConnectionNullException("Null connection");
		}
	}
	

}
