package com.iquest.university.Week13P1Balea;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	private String serverName;
	private String databaseName;
	private String username;
	private String password;
	private Connection connection;
	private String pathToConfigFile ;

	public DBConnection(String configurationFile) {
		pathToConfigFile = this.getClass().getClassLoader().getResource(configurationFile).getPath();
		setConfigurations();
		try {
			enstablishConnection();
		} catch (SQLException e) {
			///EXCEPTIE CREDENTIALS
		}
	}
	
	private void setConfigurations() {
		String[] configurations = getConfigurationFileFromFile();
		serverName = configurations[0];
		databaseName = configurations[1];
		username = configurations[2];
		if (configurations.length == 4) {
			password = configurations[3];
		}
		else {
			password = "";
		}
	}
	
	private String[] getConfigurationFileFromFile() {
		String[] configurations = null; 
		try {
			configurations = ReadFromFile.readLinesFromTextFile(pathToConfigFile);
		} catch (IOException e) {
			////EXCEPTIEEEEE Invalid file
			e.printStackTrace();
		}
		return configurations;
	}
	
	private void enstablishConnection() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		String url = "jdbc:mysql://" + serverName + "/" + databaseName;
		connection = DriverManager.getConnection(url, username, password);
	}

	public Connection getConnection() {
		return connection;
	}
	
	
}
