package exception;

public class CannotOperateWithConnectionException extends RuntimeException {

	public CannotOperateWithConnectionException(String message) {
		super(message);
	}
}
