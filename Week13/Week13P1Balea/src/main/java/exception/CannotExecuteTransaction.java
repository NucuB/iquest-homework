package exception;

public class CannotExecuteTransaction extends RuntimeException {

	public CannotExecuteTransaction(String message) {
		super(message);
	}
}
