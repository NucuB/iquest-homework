package exception;

public class ConnectionNullException extends RuntimeException {

	public ConnectionNullException(String message) {
		super(message);
	}
}
