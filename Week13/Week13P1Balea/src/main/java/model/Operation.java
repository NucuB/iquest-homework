package model;

public class Operation {

	private int amount;
	private int userId;
	private String description;
	
	public Operation(int amount, String description, int userId) {
		this.amount = amount;
		this.description = description;
		this.userId = userId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
