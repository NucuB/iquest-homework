package com.iquest.university.rl.Week15P1Balea;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class EventManagementTest {

	private IEventManagement eventManagement;
	
	@Before
	public void setUp() {
		eventManagement = new EventManagement();
		Event event1 = new Event("Remote Learning C++", "2018-04-25 17:20:00", "2018-04-27 16:10:00", "Somesului");
		Event event2 = new Event("Remote Learning C#", "2018-06-25 17:20:00", "2018-07-27 16:10:00", "Somesului nr. 9");
		Event event3 = new Event("Remote Learning Java", "2018-02-20 17:20:00", "2018-03-27 16:10:00", "Somesului nr. 10");
		Event event4 = new Event("Internship Java", "2019-05-25 17:20:00", "2019-05-27 16:10:00", "Somesului nr. 10");
		List<Event> events = Arrays.asList(event1, event2, event3, event4);
		eventManagement.setEvents(events);
		
	}
	
	@Test 
	public void testUserGivesADateAndTimeZoneShouldReturnAllEventsOnThatDate() {
		String givenDate = "2018-02-20 17:20:00";
		String timeZone = "Europe/Dublin";
		List<Event> resultList = eventManagement.getEventsTakingPlaceOnGivenDateAndTimeZone(givenDate, timeZone);
		int actualSize = resultList.size();
		int expectedSize = 1;
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void testUserGivesDateIntervalShouldGetAllEventsInSpecifiedInterval() {
		String beginningTime = "2018-01-01 00:00:00";
		String endTime = "2018-05-01 00:00:00";
		List<Event> resultList = eventManagement.getEventsTakingPlaceInGivenTimeInterval(beginningTime, endTime);
		int actualSize = resultList.size();
		int expectedSize = 2;
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void testGetAllEventsNextWeekend() {
		List<Event> eventsNextWeekend = eventManagement.getEventsTakingPlaceNextWeekend();
		int actualSize = eventsNextWeekend.size();
		int expectedSize = 1;
		assertEquals(expectedSize, actualSize);
	}
}
