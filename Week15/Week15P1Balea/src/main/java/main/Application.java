package main;

import java.util.List;
import java.util.Scanner;

import com.iquest.university.rl.Week15P1Balea.Event;
import com.iquest.university.rl.Week15P1Balea.EventManagement;
import com.iquest.university.rl.Week15P1Balea.IEventManagement;

public class Application {

	public static void main(String[] args) {

		System.out.println("Welcome to event management application !");
		IEventManagement eventManagemenet = new EventManagement();
		while (eventManagemenet.isRunning()) {

			eventManagemenet.initConsoleInterface();
			Scanner scanner = new Scanner(System.in);
			String choosenOption = scanner.nextLine();
			switch (choosenOption) {
			case "1":
				eventManagemenet.addEvent();
				break;
				
			case "2":
				List<Event> eventsNextWeekend = eventManagemenet.getEventsTakingPlaceNextWeekend();
				System.out.println(eventsNextWeekend.toString());
				break;
				
			case "3":
				System.out.println("Enter the date you are interested in : ");
				String givenDate = scanner.nextLine();
				System.out.println("Specify the timezone : ");
				String timeZone = scanner.nextLine();
				List<Event> eventsInGivenDateAndTimeZone = eventManagemenet.getEventsTakingPlaceOnGivenDateAndTimeZone(givenDate, timeZone);
				System.out.println(eventsInGivenDateAndTimeZone.toString());
				break;
				
			case "4":
				System.out.println("Enter the beginning date of interval : ");
				String beginningDate = scanner.nextLine();
				System.out.println("Enter the end date of interval : ");
				String endDate = scanner.nextLine();
				List<Event> eventsInGivenInterval = eventManagemenet.getEventsTakingPlaceInGivenTimeInterval(beginningDate, endDate);
				System.out.println(eventsInGivenInterval.toString());
				break;
				
			case "5":
				eventManagemenet.setRunning(false);
				scanner.close();
				break;
				
			default :
				System.out.println("You have entered an invalid command");
				break;
			}
		}

	}

}
