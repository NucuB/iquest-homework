package com.iquest.university.rl.Week15P1Balea;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EventManagement implements IEventManagement {

	private final static DayOfWeek SATURDAY = DayOfWeek.SATURDAY;
	private final static DayOfWeek SUNDAY = DayOfWeek.SUNDAY;
	
	private List<Event> events;
	private boolean running;
	private Scanner scanner;
	private DateTimeFormatter defaultFormatter;

	public EventManagement() {
		scanner = new Scanner(System.in);
		running = true;
		events = new ArrayList<Event>();
		defaultFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	}

	@Override
	public void addEvent() {
		System.out.println("Enter event description : ");
		String summary = scanner.nextLine();
		System.out.println("Enter start date of event (Use yyyy-MM-dd HH:mm:ss format) : ");
		String startDateText = scanner.nextLine();
		LocalDateTime startDate = LocalDateTime.parse(startDateText, defaultFormatter);
		System.out.println("Enter end date of event (Use yyyy-MM-dd HH:mm:ss format) : ");
		String endDateText = scanner.nextLine();
		LocalDateTime endDate = LocalDateTime.parse(endDateText, defaultFormatter);
		System.out.println("Enter location of event : ");
		String location = scanner.nextLine();
		Event event = new Event(summary, startDate, endDate, location);
		addEventIntoList(event);
	}

	@Override
	public List<Event> getEventsTakingPlaceNextWeekend() {
		List<Event> eventsNextWeekend = new ArrayList<Event>();
		LocalDateTime currentTime = LocalDateTime.now();
		List<LocalDateTime> weekendDates = getWekendDays(currentTime);
		LocalDateTime dateOnSaturday = weekendDates.get(0);
		LocalDateTime dateOnSunday = weekendDates.get(1);
		for (Event event : events) {
			if (checkTwoDatesAreEqual(event.getStartTime(), dateOnSaturday)
					|| checkTwoDatesAreEqual(event.getStartTime(), dateOnSunday)) {
				eventsNextWeekend.add(event);
			}
		}
		return eventsNextWeekend;
	}

	@Override
	public List<Event> getEventsTakingPlaceOnGivenDateAndTimeZone(String givenDate, String timeZone) {
		List<Event> eventsOnGivenDate = new ArrayList<Event>();
		ZonedDateTime zonedGivenDateTime = ZonedDateTime.of(LocalDateTime.parse(givenDate, defaultFormatter),
				ZoneId.of(timeZone));

		for (Event event : events) {
			ZonedDateTime dateTimeWithTimeZone = ZonedDateTime.of(event.getStartTime(), ZoneId.of(timeZone));
			if (dateTimeWithTimeZone.equals(zonedGivenDateTime)) {
				eventsOnGivenDate.add(event);
			}
		}
		return eventsOnGivenDate;
	}

	@Override
	public List<Event> getEventsTakingPlaceInGivenTimeInterval(String startTime, String endTime) {
		List<Event> eventsInGivenInterval = new ArrayList<Event>();
		LocalDateTime startDateTimeInterval = LocalDateTime.parse(startTime, defaultFormatter);
		LocalDateTime endDateTimeInterval = LocalDateTime.parse(endTime, defaultFormatter);
		for (Event event : events) {
			if (event.getStartTime().isAfter(startDateTimeInterval)
					&& event.getEndTime().isBefore(endDateTimeInterval)) {
				eventsInGivenInterval.add(event);
			}
		}
		return eventsInGivenInterval;
	}

	@Override
	public void initConsoleInterface() {
		System.out.println("What action would you like to perform ? ");
		System.out.println("Press 1 --> Add Event");
		System.out.println("Press 2 --> See what events are taking place next week");
		System.out.println("Press 3 --> Enter a date and a time zone and see what events are taking place at that time");
		System.out.println("Press 4 --> Enter a date time interval and see which event are taking place");
		System.out.println("Press 5 --> Exit application");
	}

	@Override
	public boolean isRunning() {
		return running;
	}

	@Override
	public void setRunning(boolean running) {
		this.running = running;
	}

	@Override
	public List<Event> getEvents() {
		return events;
	}

	@Override
	public void setEvents(List<Event> events) {
		this.events = events;
	}

	private void addEventIntoList(Event event) {
		events.add(event);
	}

	private boolean checkTwoDatesAreEqual(LocalDateTime firstDate, LocalDateTime secondDate) {
		if (firstDate.getYear() == secondDate.getYear() && firstDate.getMonthValue() == secondDate.getMonthValue()
				&& firstDate.getDayOfMonth() == secondDate.getDayOfMonth()) {
			return true;
		}
		return false;
	}
	
	private List<LocalDateTime> getWekendDays(LocalDateTime currenTime) {
		LocalDateTime nextSaturday;
		LocalDateTime nextSunday; 
		List<LocalDateTime> weekend = new ArrayList<LocalDateTime>();
		if (currenTime.getDayOfWeek() == SATURDAY || currenTime.getDayOfWeek() == SUNDAY) {
			LocalDateTime auxiliary = currenTime.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
			nextSaturday = auxiliary.with(TemporalAdjusters.next(SATURDAY));
			nextSunday = auxiliary.with(TemporalAdjusters.next(SUNDAY));
			weekend.add(nextSaturday);
			weekend.add(nextSunday);
		} else {
			nextSaturday = currenTime.with(TemporalAdjusters.next(SATURDAY));
			nextSunday = currenTime.with(TemporalAdjusters.next(SUNDAY));
			weekend.add(nextSaturday);
			weekend.add(nextSunday);
		}
		return weekend;
	}

}
