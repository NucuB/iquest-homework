package com.iquest.university.rl.Week15P1Balea;

import java.util.List;

public interface IEventManagement extends IManagement {

	void addEvent();
	void setEvents(List<Event> list);
	List<Event> getEvents();
	List<Event> getEventsTakingPlaceNextWeekend();
	List<Event> getEventsTakingPlaceOnGivenDateAndTimeZone(String givenDate, String timeZone);
	List<Event> getEventsTakingPlaceInGivenTimeInterval(String startTime, String endTime);
}
