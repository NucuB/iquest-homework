package com.iquest.university.rl.Week15P1Balea;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Event {

	private String summary;
	private String location;
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	

	public Event(String summary, LocalDateTime startTime, LocalDateTime endTime, String location) {
		this.summary = summary;
		this.startTime = startTime;
		this.endTime = endTime;
		this.location = location;
	}
	
	//For unit tests
	public Event(String summary, String startTime, String endTime, String location) {
		this.summary = summary;
		this.startTime = LocalDateTime.parse(startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		this.endTime = LocalDateTime.parse(endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		this.location = location;
	}

	public String getSummary() {
		return summary;
	}

	public String getLocation() {
		return location;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	@Override
	public String toString() {
		String returnedString =  "Event : " + summary + " starts at : " + startTime.toString() + " until : " + endTime + " at : "
				+ location;
		return returnedString.replace("T", " ");

	}

}
