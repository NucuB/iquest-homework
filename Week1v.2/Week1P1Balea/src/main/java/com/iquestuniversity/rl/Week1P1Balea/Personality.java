package com.iquestuniversity.rl.Week1P1Balea;

public class Personality {

	private String firstName;
	private String lastName;
	private String dateOfBirth;
	private String dateOfDeath;

	public Personality(String firstName, String lastName, String dateOfBirth, String dateOfDeath) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.dateOfDeath = dateOfDeath;
	}

	public Personality(String firstName, String lastName, String dateOfBirth) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.dateOfDeath = null;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	@Override
	public String toString() {

		if (dateOfDeath == null) {
			return firstName + " " + lastName + " (" + dateOfBirth + ") ";
		}
		return firstName + " " + lastName + " ( " + dateOfBirth + " - " + dateOfDeath + ") ";
	}

	@Override
	public boolean equals(Object object) {

		if (object == null) {
			return false;
		}

		if (!(object instanceof Personality)) {
			return false;
		}

		if (object == this) {
			return true;
		}

		Personality personality = (Personality) object;

		return personality.getFirstName().equals(firstName) && personality.getLastName().equals(lastName);
	}

	@Override
	public int hashCode() {

		int result = 1;
		result = result + firstName.hashCode();
		result = result + lastName.hashCode();
		return result;
	}

}
