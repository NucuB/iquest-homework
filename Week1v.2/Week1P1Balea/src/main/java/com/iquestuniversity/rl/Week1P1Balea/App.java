package com.iquestuniversity.rl.Week1P1Balea;

import java.io.IOException;

public class App {

	public void start() throws IOException {
		PersonalityGroup myGroup = new PersonalityGroup();
		myGroup.fillPersonalityGroup();
		myGroup.showPersonalitiesGroup();
	}

	public static void main(String[] args) throws IOException {
		App application = new App();
		application.start();
	}
}
