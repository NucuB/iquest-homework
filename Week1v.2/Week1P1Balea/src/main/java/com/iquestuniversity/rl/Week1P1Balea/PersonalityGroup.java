package com.iquestuniversity.rl.Week1P1Balea;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class PersonalityGroup {

	private static final int MAX_LENGHT_SPLIT_ARRAY = 4;

	private Set<Personality> myPersonalitySet = new HashSet<Personality>();

	public void showPersonalitiesGroup() {
		for (Personality personality : myPersonalitySet) {
			System.out.println(personality.toString());
		}
	}

	public String[] parseInputFile() throws IOException {
		String inputFilePath = this.getClass().getClassLoader().getResource("W1P1input.txt").getPath();
		String[] stringsFromFile = ReadFromFile.readLinesFromTextFile(inputFilePath);
		return stringsFromFile;
	}

	public void fillPersonalityGroup() throws IOException {

		String[] stringsFromFile = parseInputFile();
		for (String line : stringsFromFile) {
			String[] stringsAfterSplit = line.split(", ");
			if (stringsAfterSplit.length == MAX_LENGHT_SPLIT_ARRAY) {
				Personality personality = new Personality(stringsAfterSplit[0], stringsAfterSplit[1],
						stringsAfterSplit[2], stringsAfterSplit[3]);
				myPersonalitySet.add(personality);
			} else {
				Personality personality = new Personality(stringsAfterSplit[0], stringsAfterSplit[1],
						stringsAfterSplit[2]);
				myPersonalitySet.add(personality);
			}
		}
	}

}
