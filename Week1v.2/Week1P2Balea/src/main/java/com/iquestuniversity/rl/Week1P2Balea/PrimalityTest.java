package com.iquestuniversity.rl.Week1P2Balea;

public class PrimalityTest {

	public void showPrimeFromInterval(int endInterval) {
		boolean prime[] = new boolean[endInterval + 1];
		for (int indexPrimeArray = 2; indexPrimeArray <= endInterval; indexPrimeArray++) {
			prime[indexPrimeArray] = true;
		}

		for (int index = 2; index * index <= endInterval; index++) {
			if (prime[index] == true) {
				for (int indexMultiples = index * index; indexMultiples <= endInterval; indexMultiples += index) {
					prime[indexMultiples] = false;
				}

			}
		}
		for (int index = 1; index <= endInterval; index++) {

			if ((index == endInterval) && (prime[index])) {
				System.out.print(index + "-PRIME");
			} else if (index == endInterval) {
				System.out.print(index);
			} else if (prime[index]) {
				System.out.print(index + "-PRIME, ");
			} else if ((index == endInterval) && (prime[index])) {
				System.out.print(index + "-PRIME");
			} else if (index == endInterval) {
				System.out.print(index);
			} else {
				System.out.print(index + ", ");
			}

		}
	}

}
