package com.iquestuniversity.rl.Week1P4Balea;

public class ConnectionManager {

	private static final int CONNECTION_ARRAY_SIZE = 10;
	private Connection[] connectionsArray;

	public class Connection {

		private int idConnection;

		private Connection(int idConnection) {
			this.idConnection = idConnection;
		}

		public int getId() {
			return idConnection;
		}

		@Override
		public String toString() {

			return "Connection " + getId() + " exists ";
		}

	}

	public ConnectionManager() {

		connectionsArray = new Connection[CONNECTION_ARRAY_SIZE];
		populateConnectionArray();

	}

	// This is the method in ConnectionManager which creates Connection objects
	private Connection createConnection(int idConnection) {

		return new Connection(idConnection);
	}

	private void populateConnectionArray() {

		for (int index = 0; index < connectionsArray.length; index++) {

			connectionsArray[index] = createConnection(index);
			System.out.println("Connection : " + connectionsArray[index].getId() + " created ");
		}

	}

	public Connection getConnection(int connectionIndex) {

		if (connectionIndex >= CONNECTION_ARRAY_SIZE) {
			System.out.println("Connection manager runned out of connections");
			return null;
		}
		return connectionsArray[connectionIndex];

	}

}
