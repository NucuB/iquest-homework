package com.iquestuniversity.rl.Week1P4Balea;

import com.iquestuniversity.rl.Week1P4Balea.ConnectionManager.Connection;

public class App {
	public void start() {

		ConnectionManager connectionManager = new ConnectionManager();
		Connection connection;

		int index = 0;

		while (true) {
			connection = connectionManager.getConnection(index);
			System.out.println(connection.toString());

			index++;
		}
	}

	public static void main(String[] args) {
		// Connection connection1 = new Connection(1); Error : constructor is private
		App application = new App();
		application.start();

	}
}
