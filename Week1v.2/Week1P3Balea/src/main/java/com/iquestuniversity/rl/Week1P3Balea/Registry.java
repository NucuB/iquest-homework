package com.iquestuniversity.rl.Week1P3Balea;

import java.util.ArrayList;

public class Registry {

	private ArrayList<Domain> domainsList;

	public Registry() {
		domainsList = new ArrayList<Domain>();
	}

	public void createDomain(String[] domainInfoFromRegistrar) {

		boolean readyToCreate = true;
		for (Domain domain : domainsList) {
			if (domainInfoFromRegistrar[0] == domain.getNameDomain()) {

				System.out.println("The domain : " + domainInfoFromRegistrar[0] + "can't be created.."
						+ "This name already exists");
				readyToCreate = false;
				break;
			} else if (domainInfoFromRegistrar[2] == domain.getHost()) {

				System.out.println("The domain with IP : " + domainInfoFromRegistrar[2] + "can't be created.."
						+ "This IP already exists");
				readyToCreate = false;
				break;
			}

		}
		if (readyToCreate) {
			System.out.println("The domain was succesfuly created! Domain name : " + domainInfoFromRegistrar[0]);
			addDomain(new Domain(domainInfoFromRegistrar[0], domainInfoFromRegistrar[1], domainInfoFromRegistrar[2]));
		}
	}

	public void addDomain(Domain addedDomain) {
		domainsList.add(addedDomain);
	}

	public void showDomainsList() {
		for (Domain domain : domainsList) {
			System.out.println(domain.toString());
		}
	}

}
