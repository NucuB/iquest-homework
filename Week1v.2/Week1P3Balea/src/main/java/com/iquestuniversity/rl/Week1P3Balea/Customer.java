package com.iquestuniversity.rl.Week1P3Balea;

public class Customer {

	private static final int MAX_DOMAIN_INFO_LENGHT = 3;
	private String[] domainInfo;

	public Customer(String domainName, String ownerName, String hostIP) {

		domainInfo = new String[MAX_DOMAIN_INFO_LENGHT];
		domainInfo[0] = domainName;
		domainInfo[1] = ownerName;
		domainInfo[2] = hostIP;

	}

	public String[] getDomainInfo() {
		return domainInfo;
	}

	public void contactReseller(Reseller reseller) {
		reseller.setDomainInfoFromCustomer(domainInfo);
	}

}
