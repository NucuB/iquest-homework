package com.iquestuniversity.rl.Week1P3Balea;

public class Domain {

	private String nameDomain;
	private String owner;
	private String host;

	public Domain(String name, String owner, String host) {
		this.nameDomain = name;
		this.owner = owner;
		this.host = host;
	}

	public String getNameDomain() {
		return nameDomain;
	}

	public String getOwner() {
		return owner;
	}

	public String getHost() {
		return host;
	}

	public void setNameDomain(String nameDomain) {
		this.nameDomain = nameDomain;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setHost(String host) {
		this.host = host;
	}

	@Override
	public String toString() {
		return "Domain name : " + getNameDomain() + " , with owner : " + getOwner() + " , and host : " + getHost();
	}

}
