package com.iquestuniversity.rl.Week1P3Balea;

public class Reseller {

	private String[] domainInfoFromCustomer;

	public String[] getDomainInfoFromCustomer() {
		return domainInfoFromCustomer;
	}

	public void setDomainInfoFromCustomer(String[] domainInfoCustomer) {
		this.domainInfoFromCustomer = domainInfoCustomer;
	}

	public void contactRegistrar(Registrar registrar) {

		registrar.setDomainInfoFromReseller(domainInfoFromCustomer);

	}

}
