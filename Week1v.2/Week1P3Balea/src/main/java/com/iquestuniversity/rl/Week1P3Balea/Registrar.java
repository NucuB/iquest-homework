package com.iquestuniversity.rl.Week1P3Balea;

public class Registrar {

	private String[] domainInfoFromReseller;

	public String[] getDomainInfoFromReseller() {
		return domainInfoFromReseller;
	}

	public void setDomainInfoFromReseller(String[] domainInfoFromReseller) {
		this.domainInfoFromReseller = domainInfoFromReseller;
	}

	public void talkToRegistry(Registry registry) {

		registry.createDomain(domainInfoFromReseller);
	}

}
