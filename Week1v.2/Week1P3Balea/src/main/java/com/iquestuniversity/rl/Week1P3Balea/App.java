package com.iquestuniversity.rl.Week1P3Balea;

public class App {
	public void start() {

		Domain domain1 = new Domain("Apps", "Cummins", "192.168.1.1");
		// Domain1 Name : Apps , Owner : Cummins , Host : 192.168.1.1
		Domain domain2 = new Domain("Challenge", "Flora", "198.162.1.2");

		Registry registry = new Registry();
		registry.addDomain(domain1);
		registry.addDomain(domain2);// Adding some random domains into registry's domains list

		Customer customer = new Customer("SuperbDomain", "Artic", "191.167.1.1");
		Reseller reseller = new Reseller();
		Registrar registrar = new Registrar();
		customer.contactReseller(reseller);
		reseller.contactRegistrar(registrar);
		registrar.talkToRegistry(registry);

		registry.showDomainsList();

	}

	public static void main(String[] args) {
		App application = new App();
		application.start();
	}
}
